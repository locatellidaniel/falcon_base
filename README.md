# Plantilla de Misiones Falcon

## Aplicación
1. Dentro del ArmA3, en el Editor, crear una misión en la isla deseada y guardarla con un nombre a elección.
1. Descargar el repositorio.
2. Extraer el contenido sobre la carpeta de misión que se esta editando (ejemplo: C:\Users\minombredeusuario\Documents\Arma 3 - Other Profiles\minickdearma\missions\mision_falcon_en_desarrollo.Altis\)

## Estructura de directorios 

Para facilitar actualizaciones de la plantilla, se sugiere NO modificar init.sqf ni description.ext. 

Si es necesario agregar funcionalidad crear los archivos respectivos en el directorio local\. Ver local\leeme.txt para mas detalles.

## Modo DEBUG (desarrollo)

El modo desarrollo permite visualizar mensajes informativos acerca del funcionamiento de la plantilla.

Para activar el modo desarrollo: crear el archivo ```debug.enabled``` en el directorio de la misión. El archivo puede tener cualquier contenido.

Para desactivar el modo desarrollo: renombrar el archivo o bien eliminarlo.
