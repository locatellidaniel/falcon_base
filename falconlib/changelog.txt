
******
v0.3.3

Nuevo:
- falcon_fnc_allowTeleport. Permite configurar una unidad para que pueda teletransportarse durante toda la partida.
- falcon_fnc_allowInvisible. Permite configurar una unidad para que pueda hacerse visible/invisible durante toda la partida.
- Intro. Agregado parámetro para mostrar texto final de la intro.

Fix/refactor:
- Briefing. Corregidas acentuaciones de las secciónes de briefing.
- Teleport. Teleport es ahora una setting dinámica.


******
v0.3.2

Nuevo:
- falcon_fnc_spawnOnce. Permite crear unidades/objetos de manera aleatoria.
- falcon_fnc_followNearestPlayer. Hace que un grupo siga al jugador más cercano.
- falcon_fnc_playersInVehicle. Comprueba si hay jugadores dentro de un vehículo (util para condiciones de waypoints, por ejemplo)
- Briefing_*.txt. Archivos de ejemplo para escribir el briefing en archivos de texto (opcional)

Fix/refactor:
- aiSpawn. Fix. En ciertos casos se elimina el spawn antes de que los jugadores salgan del área.
- ModoDios. Ahora en CBA settings, con funcionalidad dinámica activable / desactivable en misión.
- Intro. Fix. ajustado tiempos de inicio.


******
v0.3.1

Fix/refactor:
- Sistema de rehenes. Fix. Reposicionar rehen al liberarlo

******
v0.3

Nuevo:
- Sistema de rehenes.
- aiSpawn. Posibilidad de spawn con vehículos de transporte.
- falcon_fnc_intro. Intro básica.
- falcon_fnc_miscAngryMob. Agrega comportamiento agresivo a un grupo de civiles.
- Nuevas funciones auxiliares: falcon_fnc_nearestPlayer, falcon_fn_playerIsAdmin, falcon_fnc_getAdmin, falcon_fnc_playersInArea

Fix/refactor:
- Activar/Desactivar debug en tiempo de ejecución.
- Fix. No permitir rating negativo (jugadores considerados enemigos tras matar aliados).


******
v0.2.2

Nuevo:
- Deshabilitar comunicaciones para jugadores incapacitados.

Fix/refactor:
- Deshabilitado ejecución en servidor de aiSpawn.
- Remover PRC343 inicial.


******
v0.2.1

Nuevo:
- Teleport remoto: Administradores ahora tienen la opción de teletransportar cualquier jugador.

Fix/refactor:
- Simplificado sistema de debug.
- Eliminados directorios para script locales, son ahora opcionales. Ver local\leeme.txt


******
v0.2

Nuevo:
- aiSpawn. Módulo de spawn de ai.

Fix/refactor:
- Reestructurado archivos de scenario a "local".
- Cambiar el término "scenario" por "misión" o "local" según sea conveniente.

Deprecado:
- Quitada la dependencia y/o scripts relacionados con compatibilidad con RS.


******
v0.1.2

Fix/refactor:
- Cajas equipamento/Radio: Quitar compatibilidad con RS

Nuevo:
- falcon_fnc_copyCargo: Copiar el contenido de una caja a otra.

******
v0.1.1

Fix/refactor:
- falcon_fnc_setGodMode: compatible con RS 31.0


******
v0.1.0

Nuevo:
- Parametros por defecto del escenario. falcon_fnc_setScenarioDefaultParam, falcon_fnc_resolveScenarioParam.
- FalconIntro param.
- falcon_fnc_textToAML: convertir txt a ARMA MarkupLanguage (htmlish)

Fix/refactor:
- falcon_fnc_setGodMode: workaround, deshabilitar Sistema medico de RS cuando el parámetro está activo.
- Recuperar armamento después de respawn de BIS.

******
v0.0.3

Nuevo:
- falcon_fnc_enableInvisibleAction: Agrega opcion de menú para hacerse invisible.
- falcon_fnc_enableSpectatorAction: Agrega opcion de menú para inicializar modo de espectador.
- RS: Agregado equipamento medico nuevo de RS 25.x.

Fix/refactor:
- falcon_fnc_setGodMode: compatibilidad JIP. Puede setearse a nivel global o por player. player > global.

******
v0.0.2

Nuevo:
- falcon_fnc_setGodMode: Setea modo dios al jugador.

Fix:
- falcon_fnc_enableJIPAction: Agregado param extra, para ser llamado fuera de la lib.

******
v0.0.1 Primera versión

Nuevo:

- DEBUG_LOG. Macro para escribir mensajes de debug desde el servidor y desde los clientes.
- falcon_fnc_initEquipmentBox. agregar menu de accion de CajaUniformes a un objeto.
- falcon_fnc_initRadioBox. agregar menu de accion de CajaRadios a un objeto.
- falcon_fnc_populateBuildings. Crea unidades y waypoints dentro de los edificios de un área/trigger.
- falcon_fnc_setSkill. Aplica nivel de dificultad a una unidad.
- falcon_fnc_enableJIPAction. Agrega/quita accion de dialogo de teleport a un objeto/unidad.
- ambientArtillery. Script para hacer disparar a la artillería sobre un área/trigger determinado.
- DialogJIP. Dialogo de teleport para respawn/JIP.
- Notifications. hpp base para notificaciones.
- tools/export.sqf. WIP tool para exportar posiciones de objetos desde zeus a sqm.


Refactor, basados en plantillas comunes de falcon:

- Parámetros de misión: clima, distancia de visión, etc.
- HUD de nombres. Modificado para mostrar nombres sobre la cabeza.
- Dialogo CajaUniformes en base a plantilla_uniformes
- Dialogo CajaRadios en base a plantilla_uniformes
- garbageCollector. Script de remoción de unidades muertas.
