
#define DEBUG_MODULE "libInit"
#include "base.hpp"

//
// falconBaseParams
//
setTerrainGrid (["FalconTerrainGrid", 0] call BIS_fnc_getParamValue);
setViewDistance (["FalconViewDistance", 2000] call BIS_fnc_getParamValue);			// 3000
setObjectViewDistance (["FalconViewDistance", 2000] call BIS_fnc_getParamValue);	// 3000

//
// Only apply parameters that WERE defined either by:
//  1. The admin at the lobby
//  2. preInit script 
// If the param is not resolved, then do nothing: which means: use the one defined in 3den.
//
// This is of course, only valid for parameters that can be defined in editor!
//  i.e. time, clouds, fog, etc...
//


_resolvedTime = ["FalconTime", -1] call falcon_fnc_resolveScenarioParam;
if (_resolvedTime != -1) then {
    setDate [date select 0, date select 1, date select 2, _resolvedTime, 0];
};

_resolvedOvercast = ["FalconClouds", -1] call falcon_fnc_resolveScenarioParam;
if (_resolvedOvercast != -1) then {
    skipTime -24;
    86400 setOvercast (_resolvedOvercast / 100);
    skipTime 24;
};

_resolvedFog = ["FalconFog", -1] call falcon_fnc_resolveScenarioParam;
if (_resolvedFog != -1) then {
    0 = [] spawn {
    	sleep 0.1;
    	simulWeatherSync;
    	sleep 5;
    	1 setfog (_resolvedFog / 100);
    };
};

_resolvedFog = ["FalconRain", -1] call falcon_fnc_resolveScenarioParam;
if (_resolvedFog != -1) then { 
    1 setrain _resolvedFog; 
};

_resolvedWind = ["FalconWind", -1] call falcon_fnc_resolveScenarioParam;
if (_resolvedWind != -1) then {
    1 setWindstr (_resolvedWind / 100); 
};

_resolvedWaves = ["FalconWaves", -1] call falcon_fnc_resolveScenarioParam;
if (_resolvedWaves != -1) then { 
    1 setWaves (_resolvedWaves / 100);
};



if (isServer) then {
	[] execVM LIB_SCRIPT("initServer.sqf");
};

if (!isDedicated) then {
	[] execVM LIB_SCRIPT("initClient.sqf");
};
