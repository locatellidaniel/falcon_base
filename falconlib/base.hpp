
#if __has_include("..\debug.enabled")
    #define DEBUG_ENABLED true
#endif


#define LIB_VERSION "v0.3.3"

#define LIB_SCRIPT(FILE) 	("falconlib\scr\" + FILE)
#define LIB_RESOURCE(FILE)	("falconlib\rsc\" + FILE)
#define LIB_EXTERNAL(FILE) 	("external\" + FILE)
#define LIB_BASE(FILE)		("falconlib\" + FILE)

#define SCENARIO_SCRIPT(FILE) 	("local\" + FILE)
#define SCENARIO_RESOURCE(FILE) 	("local\" + FILE)

#define STRINGIFY(x) #x


#ifndef	DEBUG_MODULE
	#define DEBUG_MODULE "unkownModule"
#endif

#define DEBUG_LOG(MODULE,TEXT)		[MODULE, (TEXT)] call FALCON_fnc_debugMessage;
#define DEBUG_LOGF1(MODULE,TEXT,A)		[MODULE, (format [TEXT, A])] call FALCON_fnc_debugMessage;
#define DEBUG_LOGF2(MODULE,TEXT,A,B)		[MODULE, (format [TEXT, A, B])] call FALCON_fnc_debugMessage;
#define DEBUG_LOGF3(MODULE,TEXT,A,B,C)		[MODULE, (format [TEXT, A, B, C])] call FALCON_fnc_debugMessage;
#define DEBUG_LOGF4(MODULE,TEXT,A,B,C,D)		[MODULE, (format [TEXT, A, B, C, D])] call FALCON_fnc_debugMessage;
#define DEBUG_LOGF5(MODULE,TEXT,A,B,C,D,E)		[MODULE, (format [TEXT, A, B, C, D, E])] call FALCON_fnc_debugMessage;
#define DEBUG_LOGF6(MODULE,TEXT,A,B,C,D,E,F)		[MODULE, (format [TEXT, A, B, C, D, E, F])] call FALCON_fnc_debugMessage;

// GLOBAL namespace variables

// player/corpse namespace
#define VAR_PLAYER_PERSISTENT_TELEPORT "falcon_var_persistentTeleport"
#define VAR_PLAYER_PERSISTENT_INVISIBLE "falcon_var_persistentInvisible"
#define VAR_PLAYER_ENHANCED_REVIVE "falcon_var_enhanced_revive"

// mission namespace
#define VAR_MISSION_BALIZA "falcon_var_baliza"


#if __has_include("local\init.sqf")
#define LOCAL_INIT true
#endif

#if __has_include("local\description.hpp")
#define LOCAL_DESCRIPTION true
#endif

#if __has_include("local\CfgSounds.hpp")
#define LOCAL_CFG_SOUNDS true
#endif
