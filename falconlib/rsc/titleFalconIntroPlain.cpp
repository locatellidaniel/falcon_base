
//--- description.ext
class RscTitleFalconIntroPlain
{
    idd = -1;
    duration = 8;
    fadeIn = 1;
    fadeOut = 5;
    onLoad = "uiNamespace setVariable ['RscTitleFalconIntroPlain', _this select 0];";
    class Controls
    {
        class Image: RscPicture
        {
            style = ST_MULTI + ST_TITLE_BAR + ST_KEEP_ASPECT_RATIO;
            idc = -1;
            text = "falconlib\rsc\falcon_logo.paa";
            x = safeZoneX + (safeZoneW / 2) - (safeZoneW / 2.5) / 2;
            y = safeZoneY + (safeZoneH / 2) - (safeZoneH / 2.5) / 2;
            w = safeZoneW / 2.5;
            h = safeZoneH / 2.5;
        };
    };
};