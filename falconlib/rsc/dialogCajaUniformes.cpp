﻿
#include "dialogCajaBase.cpp"
#include "dialogCajaUniformes.hpp"

class FalconDialogCajaUniformes {
	idd = -1;
	movingEnable = true;

	onLoad = "uiNamespace setVariable ['falcon_dialogCajaUniformes', _this select 0];";
	onUnLoad = "uiNamespace setVariable ['falcon_dialogCajaUniformes', nil]";

objects[] = {};
	class controlsBackground {
		class XD_BackGround : XD_RscBackground {
			x = 0 * safezoneW + safezoneX;
	        y = 0.18 * safezoneH + safezoneY;
			w = 0.31 * safezoneW;
	        h = 0.47 * safezoneH;
			text = "";
			colorBackground[] = {0,0,0,0.3};
			sizeEx = 0.032;
		};
	};
	class controls {
		class XD_MainCaption : XD_RscText {
			x = 0.05 * safezoneW + safezoneX;
	                y = 0.09 * safezoneH + safezoneY;
			w = 0.2 * safezoneW;
	                h = 0.2 * safezoneH;
			colorBackground[] = {1, 1, 1, 0};
			text = "MENU EQUIPAMIENTO";
		};
		class XD_CancelButton: XD_RscButtonMenu {
			idc = IDC_BTN_SALIR;
			text = "Salir";
			action = "closeDialog 0";
			default = true;
			x = 0 * safezoneW + safezoneX;
	                y = 0.52 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_SeriesOne: XD_RscButtonMenu {
			idc = IDC_BTN_COGER;
			text = "Coger";
			x = 0 * safezoneW + safezoneX;
	                y = 0.47 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriestwo: XD_RscButtonMenu {
			idc = IDC_BTN_UNIFORMES;
			text = "UNIFORMES";
			x = 0.15 * safezoneW + safezoneX;
	                y = 0.47 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriesthree: XD_RscButtonMenu {
			idc = IDC_BTN_CHALECOS;
			text = "CHALECOS";
			x = 0.15 * safezoneW + safezoneX;
	                y = 0.5 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriesfour: XD_RscButtonMenu {
			idc = IDC_BTN_MOCHILAS;
			text = "MOCHILAS";
			x = 0.15 * safezoneW + safezoneX;
	                y = 0.53 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriesfive: XD_RscButtonMenu {
			idc = IDC_BTN_CASCOS;
			text = "CASCOS";
			x = 0.15 * safezoneW + safezoneX;
	                y = 0.56 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriessix: XD_RscButtonMenu {
			idc = IDC_BTN_GAFAS;
			text = "GAFAS";
			x = 0.15 * safezoneW + safezoneX;
	                y = 0.59 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriescamo: XD_RscButtonMenu {
			idc = IDC_BTN_CAMOS;
			text = "CAMO";
			x = 0.15 * safezoneW + safezoneX;
	                y = 0.62 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_List : XD_RscListBox {
			idc = IDC_LB_ITEMS;
			x = 0 * safezoneW + safezoneX;
	                y = 0.2 * safezoneH + safezoneY;
			w = 0.3 * safezoneW;
	                h = 0.25 * safezoneH;
			colorSelect[] = {0.5, 1, 0.4, 0.5};
	                colorText[] = {1, 1, 1, 1};
	                colorBackground[] = {0, 0, 0, 1};
	                colorSelect2[] = {0.5, 1, 0.4, 0.5};
	                colorSelectBackground[] = {0, 0, 0, 1};
	                colorSelectBackground2[] = {0, 0, 0, 1};
	                colorScrollbar[] = {0.2, 0.2, 0.2, 1};
	                color[] = {1, 1, 1, 1};
	                colorActive[] = {0.5, 0.5, 0.5, 0.5};
	                colorDisabled[] = {0, 0, 0, 1};
			colorPicture[] = {0, 0, 0, 0};

			colorPictureSelected[] = {0, 0, 0, 0};

			colorPictudeDisabled[] = {0, 0, 0, 0};
			onMouseButtonClick = "";

		};
		class Dom2 : XD_RscText {
			x = 0 * safezoneW + safezoneX;
	                y = 0.5 * safezoneH + safezoneY;
			w = 0.2 * safezoneW;
	                h = 0.2 * safezoneH;
			colorBackground[] = {1, 1, 1, 0};
			text = "By Falcon";
		};
	};
};
