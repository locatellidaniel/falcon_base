/*
	Falcon default params.

	ACHTUNG!	values[] can be int or float. NOT strings.
				default can only be int.
*/

class FalconDifficultyLevel
{
	title = "Nivel de dificultad";
	values[] = {1, 1.5, 2};
	texts[] = {"Facil","Medio","Dificil"};
	default = 1;
};
class FalconViewDistance
{
	title = "Distancia de vision";
	values[] = {500,1000,1500,2000,2500,3000,4000,5000,6000,10000};
	texts[] = {"500","1000","1500", "2000","2500","3000","4000","5000","6000","10000"};
	default = 1000;
};

class FalconAlwaysJIP
{
	title = "TEST: Teleport siempre disponible";
	values[] = {998,1,0};
	texts[] = {"Misión","activado", "desactivado"};
	default = 998;
};


/*class FalconReviveTime
{
	title = "Tiempo de revive";
	values[] = {100,200,300,400,500,1000};
	texts[] = {"100seg","200seg","300seg","400seg","500seg","1000seg"};
	default = 1000;
};*/
class FalconTime
{
	title = "Hora de la mision";
	values[] = {998,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
	texts[] = {"Misión", "1:00","2:00","3:00","4:00","5:00","6:00","7:00","8:00","9:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00","24:00"};
	default = 998;
};
class FalconIntro
{
	title = "Intro";
	values[] = {998,1,0};
	texts[] = {"Misión","on","off"};
	default = 998;
};
class FalconClouds
{
	title = "Nubes";
	values[] = {998,0,10,20,30,40,50,60,70,80,90,100};
	texts[] = {"Misión","0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
	default = 998;
};
class FalconFog
{
	title = "Niebla";
	values[] = {998,0,10,20,30,40,50,60,70,80,90,100};
	texts[] = {"Misión","0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
	default = 998;
};
class FalconRain
{
	title = "Lluvia";
	values[] = {998,0,10,20,30,40,50,60,70,80,90,100};
	texts[] = {"Misión","0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
	default = 998;
};
class FalconWind
{
	title = "Viento";
	values[] = {998,0,10,20,30,40,50,60,70,80,90,100};
	texts[] = {"Misión","0m/s", "10m/s","20m/s","30m/s","40m/s","50m/s","60m/s","70m/s","80m/s","90m/s","100m/s"};
	default = 998;
};
class FalconWaves
{
	title = "Olas";
	values[] = {998,0,10,20,30,40,50,60,70,80,90,100};
	texts[] = {"Misión","0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
	default = 998;
};
