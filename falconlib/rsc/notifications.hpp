
class FalconMessage
{
	title = "%1"; // Tile displayed as text on black background. Filled by arguments.
	iconPicture = "\a3\ui_f\data\GUI\Cfg\Ranks\general_gs.paa"; // Small icon displayed in left part. Colored by "color", filled by arguments.
	iconText = "%2"; // Short text displayed over the icon. Colored by "color", filled by arguments.
	description = "%3"; // Brief description displayed as structured text. Colored by "color", filled by arguments.
	color[] = {0.5,1,1,1}; // Icon and text color
	duration = 5; // How many seconds will the notification be displayed
	priority = 0; // Priority; higher number = more important; tasks in queue are selected by priority
	difficulty[] = {}; // Required difficulty settings. All listed difficulties has to be enabled
};