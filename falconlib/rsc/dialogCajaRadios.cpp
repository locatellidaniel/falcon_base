﻿
#include "dialogCajaBase.cpp"
#include "dialogCajaRadios.hpp"

class FalconDialogCajaRadios {
	idd = -1;
	movingEnable = true;

	onLoad = "uiNamespace setVariable ['falcon_dialogCajaRadios', _this select 0];";
	onUnLoad = "uiNamespace setVariable ['falcon_dialogCajaRadios', nil]";

	objects[] = {};
	class controlsBackground {
		class XD_BackGround : XD_RscBackground {
			x = 0.26 * safezoneW + safezoneX;
	        y = 0.09 * safezoneH + safezoneY;
			w = 0.44 * safezoneW;
	        h = 0.64 * safezoneH;
			text = "";
			colorBackground[] = {0,0,0,0.3};
			sizeEx = 0.032;
		};
	};
	class controls {
		class XD_MainCaption : XD_RscText {
			x = 0.28 * safezoneW + safezoneX;
	                y = 0.09 * safezoneH + safezoneY;
			w = 0.2 * safezoneW;
	                h = 0.2 * safezoneH;
			colorBackground[] = {1, 1, 1, 0};
			text = "MENU RADIOS";
		};
		class XD_CancelButton: XD_RscButtonMenu {
			idc = IDC_BTN_SALIR;
			text = "Salir";
			default = true;
			x = 0.3 * safezoneW + safezoneX;
	                y = 0.52 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_SeriesOne: XD_RscButtonMenu {
			idc = IDC_BTN_COGER;
			text = "Coger";
			x = 0.3 * safezoneW + safezoneX;
	                y = 0.47 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriestwo: XD_RscButtonMenu {
			idc = IDC_BTN_LARGAS;
			text = "LARGAS";
			x = 0.55 * safezoneW + safezoneX;
	                y = 0.47 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_Seriesthree: XD_RscButtonMenu {
			idc = IDC_BTN_CORTAS;
			text = "CORTAS";
			x = 0.55 * safezoneW + safezoneX;
	                y = 0.5 * safezoneH + safezoneY;
			w = 0.1 * safezoneW;
	                h = 0.025 * safezoneH;
		};
		class XD_List : XD_RscListBox {
			idc = IDC_LB_RADIOS;
			x = 0.28 * safezoneW + safezoneX;
	                y = 0.2 * safezoneH + safezoneY;
			w = 0.4 * safezoneW;
	                h = 0.25 * safezoneH;
			colorSelect[] = {0.5, 1, 0.4, 0.5};
	                colorText[] = {1, 1, 1, 1};
	                colorBackground[] = {0, 0, 0, 0};
	                colorSelect2[] = {0.5, 1, 0.4, 0.5};
	                colorSelectBackground[] = {0, 0, 0, .2};
	                colorSelectBackground2[] = {0, 0, 0, .2};
	                colorScrollbar[] = {0.2, 0.2, 0.2, 1};
	                color[] = {1, 1, 1, 1};
	                colorActive[] = {0.5, 0.5, 0.5, 0.5};
	                colorDisabled[] = {0, 0, 0, 1};
			colorPicture[] = {1, 1, 1, 1};
			colorPictureSelected[] = {1, 1, 1, 1};
			colorPictureDisabled[] = {1, 1, 1, 1};
			onMouseButtonClick = "";
            rowHeight = 2.75 * (((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1);

		};
		class Dom2 : XD_RscText {
			x = 0.28 * safezoneW + safezoneX;
	                y = 0.53 * safezoneH + safezoneY;
			w = 0.2 * safezoneW;
	                h = 0.2 * safezoneH;
			colorBackground[] = {1, 1, 1, 0};
			text = "By Falcon";
		};
	};
};
