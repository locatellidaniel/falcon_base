#define addf(fname) class fname {headerType = -1;}

class falcon {
	tag = "falcon";
	class falconFunctions {
		file = "falconlib\scr\fnc";

		// Internal or advanced functions
		addf(populateBuildings);
		addf(setSkill);
		addf(setGodMode);
		addf(getRandomCoordinates);
		addf(enableSpectatorAction);
		addf(server_setInvisible);
		addf(spectatorMode);
		addf(initializeBackpack);
		addf(setScenarioDefaultParam);
		addf(resolveScenarioParam);
		addf(textToAML);
		addf(debugMessage);
		addf(copyCargo);
		addf(namedParams);
		addf(removeGrenades);
		addf(teleportPlayer);
		addf(nearestPlayer);
		addf(playersInArea);
		addf(getAdmin);
		addf(playersInVehicle);
		addf(toggleTeleportAction);
		addf(toggleInvisibleAction);


		// External functions, meant to be used by Mission Editors directly.
		
		// In units init field
		addf(allowTeleport);
		addf(allowInvisible);
		addf(miscAngryMob);
		addf(spawnOnce);
		addf(initRadioBox);
		addf(initEquipmentBox);
		addf(followNearestPlayer);

		// In a special waypoint
		addf(heliSmoothApproach);

		// in local/init.sqf
		addf(intro);
		addf(playerIsAdmin);

		// WIP
		addf(missionControlAdd);
		addf(missionControlStart);
	};

};
