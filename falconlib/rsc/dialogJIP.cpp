
#include "bisBaseControls.cpp"
#include "dialogJIP.hpp"

class FalconDialogJIP {
	idd = -1;
	movingEnable = false;

	onLoad = "uiNamespace setVariable ['falcon_dialogJIP', _this select 0];";
	onUnLoad = "uiNamespace setVariable ['falcon_dialogJIP', nil]";

	objects[] = {};

	controlsBackground[] = { }; 

	class controls {
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT START (by JohnMisil, v1.063, #Reduwu)
		////////////////////////////////////////////////////////
		class bkgBackground: IGUIBack
		{
			idc = IDC_FALCONJIP_BKGBACKGROUND;
			x = 4.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1 * GUI_GRID_H + GUI_GRID_Y;
			w = 31.1 * GUI_GRID_W;
			h = 19.2 * GUI_GRID_H;
		};

		class btnOk: RscButton
		{
			idc = IDC_FALCONJIP_BTNOK;
			text = "OK"; //--- ToDo: Localize;
			x = 10 * GUI_GRID_W + GUI_GRID_X;
			y = 19 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class btnCancel: RscButton
		{
			idc = IDC_FALCONJIP_BTNCANCEL;
			text = "Cancelar"; //--- ToDo: Localize;
			x = 22 * GUI_GRID_W + GUI_GRID_X;
			y = 19 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
        
        class selectPlayer: RscCombo
		{
			idc = IDC_FALCONJIP_PLAYERS_COMBO;
			text = "Jugador"; //--- ToDo: Localize;
			x = 5 * GUI_GRID_W + GUI_GRID_X;
			y = 2 * GUI_GRID_H + GUI_GRID_Y;
			w = 10 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
        
		class mapSelectJIP: RscMapControl
		{
			idc = IDC_FALCONJIP_MAPSELECTJIP;

			x = 5 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 30 * GUI_GRID_W;
			h = 13 * GUI_GRID_H;
		};
		class txtLegend: RscText
		{
			idc = IDC_FALCONJIP_TXTLEGEND;
			text = "Marca un punto en el mapa y presiona OK"; //--- ToDo: Localize;
			x = 5 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 30 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////
	};

};
