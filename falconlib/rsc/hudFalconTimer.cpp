// Falcon timer HUD.



class hudFalconTimer {
	idd = -1; 
	movingEnable = false; 
	controlsBackground[] = { }; 
	objects[] = { }; 
	name="hudFalconTimer";
	
	class controls { 
		class txtFalconTimer {
			type = CT_STATIC;
			style = ST_CENTER;
			idc = 1001;
			text = "00:00";
			x = 0.4625 * safezoneW + safezoneX;
			y = 0.02 * safezoneH + safezoneY;
			w = 0.08 * safezoneW;
			h = 0.04 * safezoneH;
			font = "PuristaMedium";
			colorBackground[] = {0.05,0.05,0.05,0.3};
			colorText[] = {1,1,1,1};
			sizeEx = 0.05;
		};
 	};

	duration = 999999;
	
	onLoad = "uiNamespace setVariable [""FALCON_HUD_TIMER"", _this select 0];";
	onUnLoad = "uiNamespace setVariable ['FALCON_HUD_TIMER', nil]";
	
};