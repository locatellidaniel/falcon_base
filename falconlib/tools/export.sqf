
#define QUOTE(A) #A
#define GV(VAR) jed_##VAR##
#define QGV(VAR) QUOTE(GV(VAR))

#define ZEUS_DISPLAY 312
#define ZEUS_LEFT_GUI_GROUP 453

GV(charNewLine) = toString [0x0D, 0x0A];
GV(charTab) = toString [0x09];
GV(charCodeTab) = 0x09;


GV(fnc_parseFind) = {
	params ["_str", "_lines", "_start"];
	private ["_i"];

	_i = 0;
	while { _i < count _lines} do {
		scopeName "parseFind";
		if (_str == _line) then {
			breakOut "parseFind";
		};
	};

	_i;
};

GV(fnc_trimLeft) = {
	params ["_line"];
	private ["_arr", "_i", "_len"];
	_arr = toArray _line;
	_len = count _arr;
	diag_log str _arr;
	// trim spaces
	_i = 0;
	while { (_i < _len) && ((_arr select _i) == GV(charCodeTab)) } do {	_i = _i + 1; };
	_line = [_line, _i] call BIS_fnc_trimString;

	_line;
};

GV(fnc_vehicleToSQM) = {
	params ["_v", "_index", "_id"];
	private ["_out", "_init", "_temp", "_nl"];
	_nl = GV(charNewLine);

	_out = format ["class Item%1 {", _index] + _nl;

	_temp = getPosASL _v;
	_out = _out + format ["position[]={%1,%2,%3};", _temp select 0, _temp select 2, _temp select 1] + _nl;
	_init = format ["this setPosASL %1;", _temp];

	_out = _out + format ["azimut=%1;", direction _v] + _nl;
	_out = _out + format ["id=%1;", _id] + _nl;

	_temp = if ((side _v) == civilian) then { "EMPTY"} else { side _v };
	_out = _out + format ['side="%1";', _temp] + _nl;

	_out = _out + format ['vehicle="%1";', typeOf _v] + _nl;
	_out = _out + format ["skill=%1;", 0.6] + _nl;

	_temp = vectorUp _v;
	_init = _init + format ['this setVectorUp %1;', _temp];
	if (!(simulationEnabled _v)) then { _init = _init + "this enableSimulation false;"; };
	_out = _out + format ['init="%1"', _init] + _nl;
	_out = _out + format ["};", _index] + _nl;

	_out;
};

GV(setSimulation) = {
	params ["_objs", "_enabled"];

	{ _x enableSimulation _enabled; } forEach (_objs);

	if (_enabled) then {
		systemChat format ["Enabled Simulation for %1 objects.", count _objs];
	} else {
		systemChat format ["Disabled Simulation for %1 objects.", count _objs];
	};
};


GV(exportTo) = {
	params ["_objects", "_target"];
	private ["_item", "_ids", "_output"];
	_item = 75;
	_ids = 76;
	_output = "";
	{
		_output = _output + ([_x, _item, _ids] call GV(fnc_vehicleToSQM));
		_item = _item + 1;
		_ids = _ids+1;
	} forEach _objects;


	if (_target == "clip") then {
		copyToClipboard _output;
	} else {
		systemChat "not implemented"
	};
	systemChat format ["Copied to clipboard %1 objects.", _item - 1];
};

GV(import) = {
	scopeName "import";
	params ["_data"];
	private ["_lines", "_nl", "_inBlock", "_i"];

	_lines = _data splitString GV(charNewLine);
	_count = count _lines;

	// skip until we reach root level "class Vehicles"
	_i = _lines find (GV(charTab) + "class Vehicles");
	if (_i == -1) then {
		systemChat "ERROR: Parsing SQM. Couldn't find Vehicles class.";
		breakOut "import";
	};

	// go through items in Vehicles
	_inBlock = false;

	while {_i < _count } do {
		private ["_line", "_ignore"];
		_line = [_lines select _i] call GV(fnc_trimLeft);
		_ignore = false;
		//
		if (_inBlock && (["position[]=", _line] call BIS_fnc_inString)) then {
			_ignore = true;
		};
		if (_inBlock && !_ignore && (["id=", _line] call BIS_fnc_inString)) then {
			_ignore = true;
		};
		if (_inBlock && !_ignore && (["skill=", _line] call BIS_fnc_inString)) then {
			_ignore = true;
		};

		if (_inBlock && !_ignore && (["};", _line] call BIS_fnc_inString)) then {
			private ["_vh"];
			_inBlock = false;
			// create vehicle!
			systemChat format ["Creating %1.", GV(tmp_vehicle)];
			_vh = createVehicle [GV(tmp_vehicle), [0,0,0], [], 0, "NONE"];

			call compile ([GV(tmp_init), "this", "_vh"] call CBA_fnc_replace);
			_vh setDir GV(tmp_azimut);

			(allCurators select 0) addCuratorEditableObjects [[_vh], true];

			systemChat format ["Created %1.", GV(tmp_vehicle)];
		};

		if (_inBlock && !_ignore) then {
			call compile format ["%1%2",QGV(tmp_),_line];
		};

		if (!_inBlock && (["class Item", _line] call BIS_fnc_inString)) then {
			_inBlock = true;
		};

		systemChat format["(%1): block:%2, ignore: %3", _line, _inBlock, _ignore];
		sleep .1;
		_i = _i + 1;
	};
};

//
// GUI Shortcut Keys Handlers
//

if (!isNil QGV(keyUpEventHandler)) then {
	(findDisplay ZEUS_DISPLAY) displayRemoveEventHandler ["keyUp", GV(keyUpEventHandler)];
};

GV(keyUpEventHandler) = (findDisplay ZEUS_DISPLAY) displayAddEventHandler ["keyUp", {
	params ["_display", "_dikCode", "_shift", "_ctrl", "_alt"];
	// G
	if (_dikCode == 0x22 && !_ctrl && !_alt && !_shift) then {
		[curatorSelected select 0, true] call GV(setSimulation);
	};
	// CTRL + G
	if (_dikCode == 0x22 && _ctrl && !_alt && !_shift) then {
		[curatorSelected select 0, false] call GV(setSimulation);
	};

	// CTRL + T
	// EXPORT
	if (_dikCode == 0x14 && _ctrl) then {
		[(curatorEditableObjects (allCurators select 0)), "clip"] call GV(exportTo);
	};

	// SHIFT + T
	// EXPORT
	if (_dikCode == 0x14 && _shift) then {
		[(curatorEditableObjects (allCurators select 0)), "clip"] call GV(exportTo);
	};

	// CTRL + U
	// IMPORT
	if (_dikCode == 0x16 && _ctrl) then {
		[copyFromClipboard] call GV(import);
	};


	false;
}];


//
// GUI Controls
//

if (!isNil QGV(GUI_controls)) then {
	disableSerialization;
	{ ctrlDelete _x } forEach GV(GUI_controls);
	GV(GUI_controls) = nil;
};


if (isNil QGV(GUI_controls)) then {
	private ["_display", "_curatorLeft", "_guiOriginX", "_guiOriginY"];

	openCuratorInterface;
	disableSerialization;

	_display = findDisplay ZEUS_DISPLAY;

	// reduce left zeus tree height
	_curatorLeft = _display displayCtrl ZEUS_LEFT_GUI_GROUP;
	_curatorLeft ctrlSetPosition [-0.667121,-0.349091,0.37,1];
	_curatorLeft  ctrlCommit 0;


	_guiOriginX = -0.667121;
	_guiOriginY = -0.349091 + 1.01;
	GV(GUI_controls) = [];

	_ctrl = _display ctrlCreate ["RscButton", -1];
	_ctrl ctrlSetText "Enable Sim";
	_ctrl ctrlSetTooltip "Enables simulation for selected objects (G)";
	_ctrl ctrlSetPosition [_guiOriginX,_guiOriginY,.164,.05];
	_ctrl ctrlCommit 0;
	GV(GUI_controls) pushBack _ctrl;

	_ctrl = _display ctrlCreate ["RscButton", -1];
	_ctrl ctrlSetText "Disable Sim";
	_ctrl ctrlSetTooltip "Disables simulation for selected objects (SHIFT + G)";
	_ctrl ctrlSetPosition [_guiOriginX + .167, _guiOriginY,.164,.05];
	_ctrl ctrlCommit 0;
	GV(GUI_controls) pushBack _ctrl;


	_ctrl = _display ctrlCreate ["RscButton", -1];
	_ctrl ctrlSetText "Export All";
	_ctrl ctrlSetTooltip "Export All editable objects (CTRL + T)";
	_ctrl ctrlSetPosition [_guiOriginX,_guiOriginY + .1, .164, .05];
	_ctrl ctrlCommit 0;
	GV(GUI_controls) pushBack _ctrl;

	_ctrl = _display ctrlCreate ["RscButton", -1];
	_ctrl ctrlSetText "Export Selected";
	_ctrl ctrlSetTooltip "Export selected objects (SHIFT + T)";
	_ctrl ctrlSetPosition [_guiOriginX + .167, _guiOriginY +.1,.164,.05];
	_ctrl ctrlCommit 0;
	GV(GUI_controls) pushBack _ctrl;
};