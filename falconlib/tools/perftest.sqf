//PERFTEST

#define RUNNING_HANDLE "falcon_perftest_running"
#define KEY_EH falcon_perftest_eventhandler
#define ACTIVE_SCRIPTS falcon_perftest_activeScripts
#define ACTIVE_SCRIPTS_COUNT falcon_perftest_activeScriptsCount

if (!isNil RUNNING_HANDLE) then {
	terminate (missionNamespace getVariable [RUNNING_HANDLE, 0]);

	(findDisplay 46) displayRemoveEventHandler ["keyUp", KEY_EH];
};


systemChat "Perftesting!";

private _h = [] spawn {
	ACTIVE_SCRIPTS = [];
	ACTIVE_SCRIPTS_COUNT = [];

	while { true; } do {
		if (diag_fpsmin < 16) then {
			systemChat "Logging lowfps";
			{
				private _script = (_x select 0);

				if (_x select 2) then {
				private _index = ACTIVE_SCRIPTS find _script;

				if (_index >= 0) then {
					ACTIVE_SCRIPTS_COUNT set [_index, (ACTIVE_SCRIPTS_COUNT select _index) + 1];
				} else {
					_index = count ACTIVE_SCRIPTS;
					ACTIVE_SCRIPTS set [_index, _script];
					ACTIVE_SCRIPTS_COUNT set [_index, 1];
				};
				};

			} forEach diag_activeSQsScripts;
		};
		sleep 1;
	};
};

KEY_EH = (findDisplay 46) displayAddEventHandler ["keyUp", {
	params ["_display", "_dikCode", "_shift", "_ctrl", "_alt"];

	// SHIFT + T
	// EXPORT
	if (_dikCode == 0x14 && _shift) then {
		private _sorted = [ACTIVE_SCRIPTS,[],{
			private _index = ACTIVE_SCRIPTS find _x;
			ACTIVE_SCRIPTS_COUNT select _index;
		},"DESCEND"] call BIS_fnc_sortBy;

		{
			private _index = ACTIVE_SCRIPTS find _x;
			diag_log ((str _x) + "[" + str (ACTIVE_SCRIPTS_COUNT select _index) + "]");
		} forEach _sorted;

	};


	true;
}];


missionNamespace setVariable [RUNNING_HANDLE, _h];