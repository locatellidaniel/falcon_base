
#define DEBUG_MODULE "aiSpawn_trackPlayerPositions"
#include "..\..\base.hpp"

#define CHUNK_DISTANCE 100
#define MAX_CHUNK 10000


#define NEIGHBOUR_OFFSETS [1*CHUNK_DISTANCE,0],[1*CHUNK_DISTANCE,1*CHUNK_DISTANCE],[1*CHUNK_DISTANCE,-1*CHUNK_DISTANCE],[0,1*CHUNK_DISTANCE],[0,-1*CHUNK_DISTANCE],[-1*CHUNK_DISTANCE,0],[-1*CHUNK_DISTANCE,1*CHUNK_DISTANCE],[-1*CHUNK_DISTANCE,-1*CHUNK_DISTANCE]

#define UPDATE_FREQUENCY 30
#define HEATMAP_CACHE "falcon_aiSpawn_heatMapCache"
#define HEATMAP_CACHE_LAST_UPDATE "falcon_aiSpawn_heatMapCache_lastUpdate"


// Memoize function. 
// Only execute if it hasnt been executed the last UPDATE_FREQUENCY seconds
private _last_update = missionNamespace getVariable [HEATMAP_CACHE_LAST_UPDATE, -1];
if (_last_update != -1 && _last_update + UPDATE_FREQUENCY > time) exitWith {
    missionNamespace getVariable HEATMAP_CACHE
};

DEBUG_LOG(DEBUG_MODULE, "Updating cache...");

private _units = call BIS_fnc_listPlayers;

// split units positions in a grid table, so all units in the same cell get into the same hash key  
//

// Convert a position to our grid hash 
_fnc_hash_position = {
    params ["_pos"];
    
    (floor ((_pos select 0) / CHUNK_DISTANCE) + (floor ((_pos select 1) / CHUNK_DISTANCE) * MAX_CHUNK));
};

// Convert a hash to a position
_fnc_reverse_hash_position = {
    params ["_hash"];
    
    [(_hash % MAX_CHUNK) * CHUNK_DISTANCE + CHUNK_DISTANCE/2, (floor(_hash / MAX_CHUNK)) * CHUNK_DISTANCE + CHUNK_DISTANCE/2];
};


// _chunks will be a hashmap, where 
//    the key is the grid hash of x,y coordinates and 
//    the value is an array of all the units on that grid
private _chunks = createHashMap;

{
    private _pos = position _x;                     // the unit position 
    private _key = [_pos] call _fnc_hash_position;  // calculate the cell hash for that position 
    
    // append to current chunk if it exists or initialize
    if (_key in _chunks) then {
        (_chunks get _key) pushBack _x;
    } else {
        _chunks set [_key, [_x]];
    };
    
} forEach _units;


private _total_units = count _units;

//
// _chunk_scores is a hashmap where each chunk gets a number related to how important 
//  that location is.
//  score is defined only by the amount of units in that cell and its neighbours.
//
private _chunk_scores = createHashMap;
{ 
    private _chunk_hash = _x;
    private _score = count _y;
    
    // check the neighbouring cells for more units and add to the score
    {
        private _neighbour_hash = ([_x] call _fnc_hash_position) + _chunk_hash;
        
        if (_neighbour_hash in _chunks) then {            
            _score = _score + count (_chunks get _neighbour_hash);
        };
    } forEach [NEIGHBOUR_OFFSETS];
    
    // set the score 
    _chunk_scores set [_chunk_hash, _score];
} forEach _chunks;

// Select the best
private _chunksAsArray = toArray _chunk_scores;

[_chunksAsArray, [], {
    _x select 1;
}, "DESCEND"] call BIS_fnc_sortBy;

_result = _chunksAsArray apply { _x call _fnc_reverse_hash_position };



//
// Debug 
//


for "_i" from 0 to 100 do {
    deleteMarker (format ["chunk_%1", _i]);
    deleteMarker (format ["mark_u_%1", _i]);
};

if (Falcon_debug_enabled) then {
    { 
        private _mark = createMarker [format ["mark_u_%1", _forEachIndex], _x];
        _mark setMarkerShape "ICON";
        _mark setMarkerType "mil_triangle";
        _mark setMarkerColor "Color4_FD_F";
        _mark setMarkerSize [1,1];
    } forEach _units;



    {     
        private _mark = createMarkerLocal [format ["chunk_%1", _forEachIndex], [_x] call _fnc_reverse_hash_position];
        _mark setMarkerShape "ICON";
        _mark setMarkerType "selector_selectable";
        _mark setMarkerColor "Color4_FD_F";
        _mark setMarkerSize [1,1];
        _mark setMarkerText format ["%2", _forEachIndex, _chunk_scores get _x];
    } forEach _chunks;
};


// memoize
missionNamespace setVariable [HEATMAP_CACHE, _result];
missionNamespace setVariable [HEATMAP_CACHE_LAST_UPDATE, time];

_result
