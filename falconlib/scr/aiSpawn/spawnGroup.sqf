/*
    Creates a group of given units in given position.
    Uses stored loadout and classes.
    
    returns the created group
*/

#define DEBUG_MODULE "spawnAI_spawnGroup"
#include "..\..\base.hpp"

DEBUG_LOG(DEBUG_MODULE, "Spawning group...");

params [
    ["_instance", [], []],
    ["_count", [], []],
    ["_pos", [], []]
];


private _classes = _instance getVariable "class_list";
private _loadouts = _instance getVariable "loadout_list";
private _group_side = _instance getVariable "group_side";
private _vehicle_classes = _instance getVariable "vehicle_list";

private _group = createGroup _group_side;

private _vehicle = objNull;
private _vehicle_positions = 0;

// create vehicle
if (count _vehicle_classes > 0) then {
    _vehicle = createVehicle [selectRandom _vehicle_classes, _pos, [], 0, "NONE"];
    _group addVehicle _vehicle;

    // check all available positions in the vehicle
    _vehicle_positions = (_vehicle emptyPositions "Driver") + (_vehicle emptyPositions "Commander") + (_vehicle emptyPositions "Gunner") + (_vehicle emptyPositions "Cargo");
    if (_count > _vehicle_positions) then {
        DEBUG_LOGF2(DEBUG_MODULE, "Reducing spawn count from %1 to %2 so they fit in vehicle.", _count, _vehicle_positions);
        _count = _vehicle_positions;
    };
};




for "_i" from 1 to _count do {
    private _unit = _group createUnit [selectRandom _classes, _pos, [], 0, "NONE"];
    _unit setUnitLoadout [selectRandom _loadouts, true];
    
    _unit call falcon_fnc_removeGrenades;
    
    // put the unit anywhere inside the vehicle
    if (!isNull(_vehicle)) then {
        _unit moveInAny _vehicle;
    };

};



_group deleteGroupWhenEmpty true;

// add time stamp for garbage collector 
_group setVariable ["time_created", time];

_group
