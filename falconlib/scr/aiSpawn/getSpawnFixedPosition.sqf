#define DEBUG_MODULE "aiSpawn_getSpawnFixedPosition"
#include "..\..\base.hpp"

// Get a valid spawn position given the instance parameters 

params [
    ["_instance", [], []]
];

private _selectedPosition = [0,0,0];


private _positions = _instance getVariable "positions";

// delete disabled positions 
private _disabled = _positions findIf { !(_x getVariable ["active", true]) };
while { _disabled != -1 } do {
    _positions deleteAt _disabled;
    DEBUG_LOGF1(DEBUG_MODULE, "Disabling position %1", _disabled);
    _disabled = _positions findIf { !(_x getVariable ["active", true]) };
};

if (count _positions < 1) exitWith { _selectedPosition = [] };

private _selected = selectRandom _positions;
private _position = getPos _selected;

// Get a random building position if the prop is active and there's a building nearby
if (_selected getVariable ["useBuildingPositions", false]) then {
    _building = nearestObject [(getPos _selected), "House"];
    
    if (!isNull _building) then {
        _position = selectRandom ([_building] call BIS_fnc_buildingPositions);
    };
};

_position
