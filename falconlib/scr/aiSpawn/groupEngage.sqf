#define DEBUG_MODULE "aiSpawn_groupEngage"
#include "..\..\base.hpp"

// Loop of an AI Group spawned with Attack order 

#define UPDATE_FREQUENCY (20 + random 5)

params [
    ["_instance", [], []],
    ["_group", [grpNull]]
];

DEBUG_LOG(DEBUG_MODULE, "Taking control of group");

// Get group vehicle
private _vehicle = objNull;
{ 
    if ((vehicle _x) != _x) then {
        _vehicle = vehicle _x;
    };
} forEach (units _group);

if (! (isNull _vehicle)) then {
    DEBUG_LOG(DEBUG_MODULE, "Group has a vehicle");
};

private _trigger = _instance getVariable ["trigger", nil];
private _disembark_distance = (_instance getVariable ["disembark_distance", [0]]) select 0;
private _disembark_distance_range = (_instance getVariable ["disembark_distance", [100]]) select 1;

while { !isNull _group } do {
    // Get sorted 2d positions (form more to less) of player clusters
    private _targets = [] call falcon_fnc_aiSpawn_trackPlayerPositions;

    private _target = []; // 2d position
    private _group_has_close_cluster = false;

    {
        // Find the first target (most players) that is in area 
        if ((count _target) == 0 && (_x inArea _trigger)) then {
            _target = _x;
        };
        // Check distance to live
        if (((leader _group) distance2D _x) < (_instance getVariable "distance_to_live") ) then {
            _group_has_close_cluster = true;
        };
    } forEach _targets;
    
    // Only attack when there is a target
    if ((count _target) != 0) then {
        [_group] call CBA_fnc_clearWaypoints;

        // convert _target to 3D (add 0 as last element)
        private _target3d = _target + [0];

        // Disembark Logic.
        //   check that dismbark distance is positive
        //   check that there's a vehicle 
        //   check distance
        if (! (isNull _vehicle) && _disembark_distance > 0 && ((leader _group) distance2d _target3d) < (_disembark_distance + random _disembark_distance_range)) then {
            _group leaveVehicle _vehicle;
        };

        [_group, _target3d] call CBA_fnc_taskAttack;
    } else {
        DEBUG_LOG(DEBUG_MODULE, "No target inside trigger area.");
    };
    
    
    sleep UPDATE_FREQUENCY;
};

DEBUG_LOG(DEBUG_MODULE, "Group deleted.");
