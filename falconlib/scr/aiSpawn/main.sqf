#define DEBUG_MODULE "aiSpawn"
#include "..\..\base.hpp"

if (!isServer) exitWith { true };

DEBUG_LOG(DEBUG_MODULE, "started.");
private _args = [createHashMapFromArray [
    ["_instance", [objNull, true]],
    ["_total_units", [40]],
    ["_interval_min", [5]],
    ["_interval_max", [10]],
    ["_count_min", [3]],
    ["_count_max", [8]],
    ["_max_active_units", [10]],
    ["_side", [east]],
    ["_order", ["defend"]],
    ["_spawn_min_distance", [400]],
    ["_spawn_max_distance", [700]],

    ["_disembark_distance", [450]],
    ["_disembark_distance_range", [100]],

    ["_time_to_live", [5 * 60]],
    ["_distance_to_live", [500]]
], _this] call falcon_fnc_namedParams;

if (!(_args isEqualType createHashMap )) exitWith {
    DEBUG_LOG(DEBUG_MODULE, "Parámetros incorrectos. Spawn desactivado.");
};
    
(values _args) params (keys _args);

// initialize global module functions only once
if (isNil "falcon_fnc_aiSpawn_initInstance") then {
    ["falconlib\scr\aiSpawn\", "falcon_fnc_aiSpawn_", [
        "initInstance",
        "spawnGroup",
        "trackPlayerPositions",
        "getSpawnFixedPosition",
        "getSpawnDynamicPosition",
        "groupEngage"
    ]] call BIS_fnc_loadFunctions;
};

// init instance vars 
if (_instance getVariable ["initialized", false]) exitWith {
    ["WARN: Falcon AI Spawn. El módulo ya se ha inicializado!"] call BIS_fnc_error; 
    false;
};

_instance setVariable ["initialized", false];
_instance setVariable ["total_units", _total_units];
_instance setVariable ["group_side", _side];
_instance setVariable ["class_list", []];
_instance setVariable ["loadout_list", []];
_instance setVariable ["spawn_distance", [_spawn_min_distance, _spawn_max_distance]];
_instance setVariable ["disembark_distance", [_disembark_distance, _disembark_distance_range]];
_instance setVariable ["time_to_live", _time_to_live];
_instance setVariable ["distance_to_live", _distance_to_live];

// exit if we couldn't initialize
if (! ([_instance] call falcon_fnc_aiSpawn_initInstance) ) exitWith { false };
    
_instance setVariable ["initialized", true];

DEBUG_LOG(DEBUG_MODULE, "Initialized.");

//["spawn", _instance] call falcon_fnc_missionControlAdd;

private _activated = _instance getVariable ["activated", false];
while {!_activated} do
{
    sleep 10;
    _activated = triggerActivated (_instance getVariable "trigger");
};

DEBUG_LOG(DEBUG_MODULE, "Activated!");
_instance setVariable ["activated", true];
private _active_units = [];
private _spawned_unit_count = 0;
private _activated_time = time;
private _trigger = (_instance getVariable "trigger");

scopeName "main";
while { _instance getVariable "activated" } do
{
    scopeName "activeLoop";
    sleep 1;

    // recreate _active_units array with only the units that area alive
    DEBUG_LOG(DEBUG_MODULE, "Cleanup started...");
    private _alive_units = [];
    { 
        if (alive _x) then { 
            _alive_units pushBack _x; 
        };
    } forEach _active_units;

    _active_units = _alive_units;

    // Break if active and no more players in area
    if (!(_trigger call falcon_fnc_playersInArea) && ((time - _activated_time) > _time_to_live)) then {
        DEBUG_LOG(DEBUG_MODULE, "No more players in AREA && TTL expired.");
        breakTo "main";
    };
    
    // Calculate how many units do we have available this cycle.
    //  the minimum between how many active units we can have, and the remaining of the total limit.
    private _available_unit_count = (_max_active_units - count _active_units) min (_total_units - _spawned_unit_count);
    DEBUG_LOGF1(DEBUG_MODULE, "Available units: %1", _available_unit_count);
    
    // Can we ever create a group with min_units?
    if (_count_min > (_total_units - _spawned_unit_count)) then { 
        DEBUG_LOG(DEBUG_MODULE, "Not enough units available for creating the last group.");
        breakTo "main";
    };
    
    // Can we create a group of the minimum number of units?
    if (_available_unit_count >= _count_min) then {
        // determine how many units we're creating 
        // random, but never more than what we have available.
        private _count = [_count_min min _available_unit_count, _count_max min _available_unit_count] call BIS_fnc_randomInt;
        if (_count > 0) then {
            
            // find good spawn position and a target position
            private _pos = [];
            private _target = [];
            if (_instance getVariable "fixed_positions") then {
                _pos = _instance call falcon_fnc_aiSpawn_getSpawnFixedPosition;
                if (count _pos < 1) exitWith {
                    DEBUG_LOG(DEBUG_MODULE, "All spawn positions have been disabled.");
                    breakTo "main";
                };
                _target = _pos;
            } else {
                _dynamic_position = _instance call falcon_fnc_aiSpawn_getSpawnDynamicPosition;
                if (count _dynamic_position < 1) exitWith {
                    DEBUG_LOG(DEBUG_MODULE, "Could not find a dynamic spawn position.");
                    breakTo "activeLoop";
                };
                _pos = _dynamic_position select 0;
                _target = _dynamic_position select 1;
            };

            
            private _group = [_instance, _count, _pos] call falcon_fnc_aiSpawn_spawnGroup;
            
            // update the _count with the actually spawned units 
            _count = count (units _group);

            if (_order == "defend") then {
                // Defend area
                [_group, _target] call CBA_fnc_taskDefend;
            } else {
                // Attack players 
                [_instance, _group] spawn falcon_fnc_aiSpawn_groupEngage;
            };
            
            
            _spawned_unit_count = _spawned_unit_count + _count; 
            
            // Add units to all available zeus modules 
            {
                _x addCuratorEditableObjects [(units _group)];
            } forEach allCurators;
            
            { 
                _active_units pushBack _x;
            } forEach units _group;
            
            DEBUG_LOGF1(DEBUG_MODULE, "Spawned %1 units", _count);
        };
        
    } else {
        DEBUG_LOG(DEBUG_MODULE, "Not enough available units.");
    };    
    
    
    // Anounce cycle stats 
    DEBUG_LOGF5(DEBUG_MODULE, "Spawn cycle. Active: %1/%2. Spawned: %3/%4. Min group: %5", count _active_units, _max_active_units, _spawned_unit_count, _total_units, _count_min);
    
    if ((_total_units - _spawned_unit_count) <= 0) then {
        DEBUG_LOG(DEBUG_MODULE, "Created all possible units.");
        breakTo "main";
    };
    
    
    // Sleep the desired interval.
    sleep ([_interval_min, _interval_max] call BIS_fnc_randomInt);

   

};
// not spawning anymore
_instance setVariable ["activated", false];

DEBUG_LOG(DEBUG_MODULE, "Deactivated.");
// wait until there are no more players in the area, and the time has expired
while { (_trigger call falcon_fnc_playersInArea) || ((time - _activated_time) < _time_to_live) } do {
    sleep 10;
};

// Cleanup
DEBUG_LOG(DEBUG_MODULE, "Cleaning up...");
{ 
    if (alive _x) then { 
        deleteVehicle _x;
    };

    sleep .25;
} forEach _active_units;

_instance setVariable ["completed", true];
DEBUG_LOG(DEBUG_MODULE, "Completed.");
// ToDo: Clear memory
