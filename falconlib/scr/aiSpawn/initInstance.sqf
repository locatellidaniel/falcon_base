
#define DEBUG_MODULE "aiSpawn_initialize"
#include "..\..\base.hpp"



params [
    ["_instance", objNull, [objNull]]
];


private _triggers = [_instance] call BIS_fnc_moduleTriggers;

if (count _triggers == 0) exitWith { 
    ["ERROR: Falcon AI Spawn. No hay un activador sincronizado."] call BIS_fnc_error; 
    false;
};

_instance setVariable ["trigger", _triggers select 0];

// collect all loadouts and delete the units 
private _loadouts = [];
private _classes = [];
private _units = [_instance, "Man", false] call BIS_fnc_synchronizedObjects;

private _vehicles = [_instance, "Car", false] call BIS_fnc_synchronizedObjects;

if (count _units == 0) exitWith { 
    ["ERROR: Falcon AI Spawn. No hay unidades sincronizadas."] call BIS_fnc_error; 
    false;
};

if (count _vehicles > 0) then { 
    DEBUG_LOGF1(DEBUG_MODULE, "Detected %1 vehicles.", count _vehicles);
};


private _vehicle_classes = [];
{        
    _vehicle_classes pushBack (typeOf _x);
    
    deleteVehicle _x;
} forEach _vehicles;

{        
    _loadouts pushBack (getUnitLoadout _x);
    _classes pushBack (typeOf _x);
    
    (group _x) deleteGroupWhenEmpty true;
    
    deleteVehicle _x;
} forEach _units;
// ToDo: Loadout filters 

_instance setVariable ["class_list", _classes];
_instance setVariable ["loadout_list", _loadouts];
_instance setVariable ["vehicle_list", _vehicle_classes];


private _positions = [_instance, "LocationArea_F"] call BIS_fnc_synchronizedObjects;

if (count _positions > 0) then {
    // Initialize positions.
    // Create a trigger to disable a position after it has been detected by players 
    
    {
        private _radius = _x getVariable ["position_radius", 50];
        private _t = createTrigger ["EmptyDetector", getPosWorld _x, false];
        _x setVariable ["active", true];
        _t setVariable ["spawn_position", _x];
        _t setTriggerActivation ["ANYPLAYER", "PRESENT", false];
        _t setTriggerArea [_radius, _radius, 0, false];
        _t setTriggerStatements 
    	[
    		"this", 
    		"(thisTrigger getVariable ""spawn_position"") setVariable [""active"", false]", 
    		""
    	];
    } forEach _positions;
    
    _instance setVariable ["positions", _positions];
    _instance setVariable ["fixed_positions", true];
} else {
    _instance setVariable ["fixed_positions", false];
};

DEBUG_LOG(DEBUG_MODULE, "Finished initialization.");

true
