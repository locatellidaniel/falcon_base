#define DEBUG_MODULE "aiSpawn_getSpawnDynamicPosition"
#include "..\..\base.hpp"

// Get a valid spawn position given the instance parameters 

params [
    ["_instance", [], []]
];

private _selectedPosition = [0,0,0];



private _range = _instance getVariable ["spawn_distance", [400,700]];
private _trigger = _instance getVariable ["trigger", nil];

// choose a random player cluster inside the area
private _targets = ([] call falcon_fnc_aiSpawn_trackPlayerPositions) call BIS_fnc_arrayShuffle;
private _target = [];
{
    if (_x inArea _trigger) then {
        _target = _x;
        break;
    };
} forEach (_targets);

if (count _target == 0) exitWith {
    DEBUG_LOG(DEBUG_MODULE, "Can't find target inside trigger area.");
    []
};


// Leave more distance from objects if spawn has vehicles
private _vehicle_classes = _instance getVariable "vehicle_list";
private _safe_pos_obj_distance = if (count _vehicle_classes > 0) then { 7 } else { 3 };


_selectedPosition = [_target, _range select 0, _range select 1, _safe_pos_obj_distance, 0, 20, 0] call BIS_fnc_findSafePos;
// ToDo: try several times until we find a non-visible from player area 


[_selectedPosition, _target]
