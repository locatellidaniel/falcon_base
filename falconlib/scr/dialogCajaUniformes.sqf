/*


	Author: JohnMisil

 */


#define DEBUG_MODULE "dialogCajaUniformes"
#include "..\base.hpp"

#include "..\rsc\dialogCajaUniformes.hpp"

// global
#define PROBADOR 			falcon_dialogCajaUniformes_probador
#define SELECTED_ROLE		falcon_dialogCajaUniformes_role
#define HIDDEN_OBJECTS		falcon_dialogCajaUniformes_hiddenObjects
#define FN_SWITCH_ROLE		falcon_dialogCajaUniformes_fnc_switchRole
#define FN_COGER			falcon_dialogCajaUniformes_fnc_coger



params ["_objectBox"];

private [
	// vars
	"_dlg"
];

//
// Global function
FN_SWITCH_ROLE = {
	params ["_role"];
	private ["_scenarioData"];

	DEBUG_LOG(DEBUG_MODULE, "switched role to " + _role);
	SELECTED_ROLE = _role;
	// get the data from scenario
	_scenarioData = preprocessFileLineNumbers (SCENARIO_SCRIPT("equipment.sqf"));
	if (_scenarioData != "") then {
		_scenarioData = compile _scenarioData;
	} else {
		DEBUG_LOG(DEBUG_MODULE, "Couldn't find equipment file: " + SCENARIO_SCRIPT("equipment.sqf"))
		_scenarioData = { []; };	// dummy function, always return empty
	};


	// Clear list and load items

	lbClear IDC_LB_ITEMS;

	{
		// get label
		private ["_text"];
		if (_x != "") then {
			_text =  [(configFile >> "CfgWeapons" >> _x), "displayName", _x] call BIS_fnc_returnConfigEntry;
		} else {
			_text = "Ninguno";
		};

		// add item
		lbAdd [IDC_LB_ITEMS, _text];
		lbSetData[IDC_LB_ITEMS, _forEachIndex, _x];

		// set pic
		lbSetPicture [IDC_LB_ITEMS,_forEachIndex,getText (configFile >>"CfgWeapons" >>_x >> "Picture")];
		lbSetPictureColor [IDC_LB_ITEMS, _forEachIndex, [1, 1, 1, 1]];
	} forEach ([_role] call _scenarioData);

	lbSetCurSel [IDC_LB_ITEMS, 0];  // default first

	//update probador
	switch (_role) do {
		case "chalecos";
		case "uniformes": {
			PROBADOR attachTo [player, [.3,2,1]];
			PROBADOR setDir 180;
		};

		case "mochilas": {

			PROBADOR attachTo [player, [-.3,-2,1]];
			PROBADOR setDir 0;
		};

		case "cascos";
		case "gafas";
		case "camos": {
			PROBADOR attachTo [player, [.1,0.7,1.7]];
			PROBADOR setDir 180;
		};
	};

	PROBADOR camCommit 0;
};

FN_COGER = {
	params ["_item"];

	// Execute appropiate action depending on role
	switch (SELECTED_ROLE) do {
		case "uniformes": {
			if (_item == "") then {
				removeUniform player;
			} else {
				player addUniform _item;
			};
		};

		case "chalecos": {
			if (_item == "") then {
				removeVest player;
			} else {
				player addVest _item;
			};
		};

		case "mochilas": {
			removeBackpack player;
			if (_item != "") then {
				player addBackPack _item;
				[] call FALCON_fnc_initializeBackpack;
			};
		};

		case "cascos": {
			if (_item == "") then {
				removeHeadgear player;
			} else {
				player addHeadgear _item;
			};
		};
		case "gafas": {
			if (_item == "") then {
				removeGoggles player;
			} else {
				player addGoggles _item;
			};
		};

		case "camos": {
			if (_item != "") then {
				// broadcast setFace to all clients
				[[player, _item], "setFace", true, true] call BIS_fnc_MP;
			};
		};
	};

};

DEBUG_LOG(DEBUG_MODULE, "started.");

// create dialog
createDialog "FalconDialogCajaUniformes";
disableSerialization;

// get dialog control
_dlg = uiNamespace getVariable "falcon_dialogCajaUniformes";

// assign event handlers

//
// UNLOAD DIALOG
_dlg displayAddEventHandler ["Unload", {
	// destroy probador
	PROBADOR cameraEffect ["terminate","back"];
	camDestroy PROBADOR;

	{ _x hideObject false; } foreach allPlayers;
    { _x hideObject false; } foreach HIDDEN_OBJECTS;

	// set volume up, again.
	0.1 fadeSound 1;

	// clean up globals
	PROBADOR = nil;

	DEBUG_LOG(DEBUG_MODULE, "unloaded");
}];


//
// BUTTONS
(_dlg displayCtrl IDC_BTN_SALIR) ctrlAddEventHandler ["ButtonClick", {
	// btn salir
	closeDialog 0;
}];

(_dlg displayCtrl IDC_BTN_UNIFORMES) ctrlAddEventHandler ["ButtonClick", {
	["uniformes"] call FN_SWITCH_ROLE;
}];

(_dlg displayCtrl IDC_BTN_CHALECOS) ctrlAddEventHandler ["ButtonClick", {
	["chalecos"] call FN_SWITCH_ROLE;
}];

(_dlg displayCtrl IDC_BTN_MOCHILAS) ctrlAddEventHandler ["ButtonClick", {
	["mochilas"] call FN_SWITCH_ROLE;
}];

(_dlg displayCtrl IDC_BTN_CASCOS) ctrlAddEventHandler ["ButtonClick", {
	["cascos"] call FN_SWITCH_ROLE;
}];

(_dlg displayCtrl IDC_BTN_GAFAS) ctrlAddEventHandler ["ButtonClick", {
	["gafas"] call FN_SWITCH_ROLE;
}];

(_dlg displayCtrl IDC_BTN_CAMOS) ctrlAddEventHandler ["ButtonClick", {
	["camos"] call FN_SWITCH_ROLE;
}];

(_dlg displayCtrl IDC_BTN_COGER) ctrlAddEventHandler ["ButtonClick", {
	[lbData [IDC_LB_ITEMS, lbCurSel IDC_LB_ITEMS]] call FN_COGER;
}];


//
// INIT
//

// Probador start
// create cam
player switchMove "amovpercmstpsnonwnondnon";
PROBADOR = "camera" camCreate (player modelToWorld [0,2,1]);
PROBADOR cameraEffect ["INTERNAL", "BACK"];


["uniformes"] call FN_SWITCH_ROLE;

// hide everything but player
HIDDEN_OBJECTS = [];
{
	if(_x != player)then{ _x hideObject true; };
} foreach allPlayers;
{
    if(_x != player) then { 
        HIDDEN_OBJECTS pushBack _x;
        _x hideObject true; 
    };
} forEach nearestObjects [player, [], 5];

// reduce sound volume.
0.1 fadeSound 0.2;
