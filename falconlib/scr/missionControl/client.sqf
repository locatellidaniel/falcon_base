with uiNamespace do { 

	private _fnc_get_mainMap = {
		((allControls (findDisplay 12)) select { (ctrlType _x == 101) })#0
	};

	private _mainDisplay = (findDisplay 12);

	private _fnc_process_updates = {
		params ["_null", "_updates"];
		systemChat format ["got update %1", _updates];
	};

	if (isServer) then {
		while {true} do {
			[nil, missionNamespace getVariable ["falcon_var_missionControl_spawnsUpdate", []]] call _fnc_process_updates;
			sleep 1;
		};
	} else {

		"falcon_var_missionControl_spawnsUpdate" addPublicVariableEventHandler _fnc_process_updates;
	};
};


/*


  pos = MAPA posWorldToScreen (position player);
  ctrl = findDisplay 12 ctrlCreate ["RscButton", -1]; 

  ctrl ctrlSetPosition [pos#0,pos#1,0.2,0.1]; 
  ctrl ctrlSetText "STIG?"; 
  ctrl ctrlCommit 0; 
  ctrl ctrlAddEventHandler [ 
   "ButtonDown", 
   {hint "STIG STIG STIG STIG STIG"} 
 ]; 

  [] spawn {
    while {true} do {
{ 

  if (ctrlType _x == 101) then {
    MAPA = _x;
  };

} forEach allControls (findDisplay 12);

      pos = MAPA posWorldToScreen (position player);
      ctrl ctrlSetPosition [pos#0,pos#1,0.2,0.1];
      ctrl ctrlCommit 0;
      sleep 0.1;
    };
  };
};*/