#define DEBUG_MODULE "initClient.sqf"
#include "..\base.hpp"





DEBUG_LOG(DEBUG_MODULE, "Removing  default radios");
player unlinkItem "ItemRadio";
player removeItems "ItemRadio";

waitUntil { alive player };



// Init
//

// Add teleport action if:
//  player did jip 
//  player has flag 
//  player is admin 
//  global setting is on 
if ( didJIP || (player getVariable [VAR_PLAYER_PERSISTENT_TELEPORT, false]) || (call falcon_fnc_playerIsAdmin) || Falcon_always_teleport) then { 
    // show 
    [player, true] call falcon_fnc_toggleTeleportAction;
};

// set invisible action 
if (player getVariable [VAR_PLAYER_PERSISTENT_INVISIBLE, false]) then {
    [player, true] call falcon_fnc_toggleInvisibleAction;
};


[] execVM LIB_SCRIPT("hud.sqf");

// restore godMode
[] call FALCON_fnc_setGodMode;




//disable dialogs
player setSpeaker "NoVoice";
player disableConversation true;

// Disable some hud info 
showHUD [ 
  true,     // Show scripted HUD such as weapon crosshair, action menu and overlays created with cutRsc and titleRsc 
  true,     // Show vehicle, soldier and weapon info 
  false,    // Show vehicle radar 
  false,    // Show vehicle compass 
  true,     // Show tank direction indicator. 
  true,     // Show commanding menu (HC related menus) 
  false,    // Show group info bar (Squad leader info bar) 
  true,     // Show HUD weapon cursors (connected with scripted HUD) 
  true,     // Show Arma 3: Custom Info 
  false,    // Show "x killed by y" systemChat messages 
  true      // Show icons drawn with drawIcon3D even when the HUD is hidden 
];


// Falcon Lib info
player createDiarySubject ["FalconLib","FalconLib"];
player createDiaryRecord ["FalconLib", ["Version", LIB_VERSION]];
player createDiaryRecord ["FalconLib", ["Changelog", [LIB_BASE("changelog.txt")] call falcon_fnc_textToAML]];

// Briefing 
{
    private _title = _x select 0;
    private _file = _x select 1;
    if (fileExists(_file)) then {
        player createDiaryRecord ["Diary",[_title, [_file] call falcon_fnc_textToAML ]];
    };
} forEach [
    // Reverse order!
    ["Inteligencia", "local\briefing_inteligencia.txt"],
    ["Misión", "local\briefing_mision.txt"], 
    ["Situación", "local\briefing_situacion.txt"]
];

// Baliza
[] spawn {
    
    while { true } do {
        scriptName "BalizaEvents";
        if (lifeState player == "INCAPACITATED" && (missionNamespace getVariable [VAR_MISSION_BALIZA, ""]) == "") then {
            // Incapacitated!
            private _mark_name = "injured_signal_" + (getPlayerUID player);
            private _mark = createMarker [_mark_name, position player];
            _mark setMarkerShape "ICON";
            _mark setMarkerType "loc_heal";
            _mark setMarkerColor "ColorRed";
            _mark setMarkerSize [1,1];
            _mark setMarkerText (name player);
            missionNamespace setVariable [VAR_MISSION_BALIZA, _mark_name];
            
            // Set ACRE spectator on
            [true] call acre_api_fnc_setSpectator;

            // Play sound everywhere
            "falcon_sound_baliza" remoteExec ["playSound", 0, false];
            
            DEBUG_LOG(DEBUG_MODULE, "Baliza created.");
        } else {
            if ((lifeState player == "HEALTHY" || lifeState player == "INJURED") && (missionNamespace getVariable [VAR_MISSION_BALIZA, ""]) != "") then {
                // Revived!
                private _mark = deleteMarker (missionNamespace getVariable VAR_MISSION_BALIZA);
                missionNamespace setVariable [VAR_MISSION_BALIZA, ""];
                
                [false] call acre_api_fnc_setSpectator;
                DEBUG_LOG(DEBUG_MODULE, "Baliza removed.");
            };
        };
        
        sleep 3;
    };
};

// this would've been the proper way to respawn jip action
player addEventHandler ["Respawn",{
    params ["_unit", "_corpse"];
    
    DEBUG_LOG(DEBUG_MODULE, "Respawn triggered. Reseting loadout, rating and JIP action")
    
    // Remove EH from corpse
    if ( _corpse getVariable [VAR_PLAYER_ENHANCED_REVIVE, -1] > -1) then {
        _corpse removeEventHandler ["HandleDamage", _corpse getVariable VAR_PLAYER_ENHANCED_REVIVE];
    };
    
	// recover loadout
    _unit setUnitLoadout (getUnitLoadout _corpse);

	// set rating to 0
	player addRating (-rating player);
    
    // Set ACRE spectator off 
    [false] call acre_api_fnc_setSpectator;

    // Hide teleport and invisible action from corpse if it was on 
    [_corpse, false] call falcon_fnc_toggleTeleportAction;
    [_corpse, false] call falcon_fnc_toggleInvisibleAction;

	// Always Show Teleport after respawn
    [player, true] call falcon_fnc_toggleTeleportAction;
    
    // set invisible action 
    if (player getVariable [VAR_PLAYER_PERSISTENT_INVISIBLE, false]) then {
        [player, true] call falcon_fnc_toggleInvisibleAction;
    };
}];

// Before revive handlers
/*if (missionNamespace getVariable ["bis_revive_mode", 0] != 0) then {
    private _eh = player addEventHandler ["HandleDamage", {
        params ["_unit", "_selection", "_damage", "_source", "_projectile", "_index"];

        if (_damage < 0.1) exitWith {};

        if (_index != -1) exitWith {};

        // For bullets reduce the chances of hit
        if (_projectile find "B_" > -1) then {
			private _damageReceived = _damage - (damage _unit);

            // 10% hit chance 
            if (true) then {
                // reduce accumulated damage, that revive system will then increase
                private _damageAccumulated = _unit getVariable ["#rev_damage", 0];
                private _damageDelta = (_damageAccumulated - _damageReceived + .0001);
                _unit setVariable ["#rev_damage", _damageDelta];
                DEBUG_LOG(DEBUG_MODULE, "Reduced damage!");
            } else {
                DEBUG_LOG(DEBUG_MODULE, "HIT!");
            };
            
        };
    }];
};*/

sleep 1; // add handlers after bis revive handlers!
if (missionNamespace getVariable ["bis_revive_mode", 0] != 0) then {
    DEBUG_LOG(DEBUG_MODULE, "Enhanced revive.");
    
    private _eh = player addEventHandler ["HandleDamage", {
        params ["_unit", "_selection", "_damage", "_source", "_projectile"];

        // Limit damage to .9
    	if (_damage > .90) then { 
            _damage = .90;
        };
        
        if (lifeState _unit == "INCAPACITATED") then {
            _damage = 0;
            // Do not allow bleed out or execution (reset vars to the minimum)
            _unit setVariable ["#rev_bleed", 0];
            if (_unit getVariable ["#rev_damage", 0] > .01) then {
                _unit setVariable ["#rev_damage", .01];
            };
        };
        
        _damage
    }];
    


    player setVariable [VAR_PLAYER_ENHANCED_REVIVE, _eh];
};

// Keep Rating
player addEventHandler ["HandleRating", {
    params ["_unit", "_rating"];

    if (_rating < 0) then { _rating = 0 };

    _rating
}];