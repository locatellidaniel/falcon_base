// initServer.sqf
//
// Se ejecuta al iniciar la misi'on en el servidor.
//

#include "..\base.hpp"

// restore godMode
[] call FALCON_fnc_setGodMode;

[] execVM LIB_SCRIPT("garbageCollector.sqf");

private _resolvedSkill = ["FalconDifficultyLevel", 1] call falcon_fnc_resolveScenarioParam;


private _skMin = .05 * _resolvedSkill;
private _skMax = .3 * _resolvedSkill;
private _skAimMin = .01 * _resolvedSkill;
private _skAimMax = .05 * _resolvedSkill;

[ 
    true, 
    [ 
        [WEST, _skMin, _skAimMin, _skMax, _skAimMax ], 
        [EAST, _skMin, _skAimMin, _skMax, _skAimMax ], 
        [RESISTANCE, _skMin, _skAimMin, _skMax, _skAimMax ]
    ]
] call BIS_fnc_EXP_camp_dynamicAISkill;

// SpawnOnce - Remove all units but one
#define SO_UNITS_VAR "falcon_var_spawnOnce"

private _so_units = missionNamespace getVariable [SO_UNITS_VAR, createHashMap];

{
    private _name = _x;
    private _units = (_so_units get _name);

    private _selected = selectRandom _units;

    {
        if (_x != _selected) then {
            deleteVehicle _x;
        };
    } forEach (_so_units get _name);

    missionNamespace setVariable [_name, _selected];    // Assign the global var 
    missionNamespace setVariable [SO_UNITS_VAR, nil];   // Delete the SO_UNITS since we don't need it anymore
} forEach (keys _so_units);


//[] call falcon_fnc_missionControlStart;

// Agregar jugadores a zeus
[] spawn {
    // Minor validation, there should only be one curator module 
    if (count allCurators > 1) then {
        ["ATENCI�N: Hay m�s de un m�dulo zeus en esta misi�n!", allCurators] call BIS_fnc_error;
    };

    // pre init
    {
        _x setVariable ["showNotification", false];
    } forEach allCurators;

    // keep zeus updated
    while {true} do {
        sleep 30;
        {
            _x addCuratorEditableObjects [allPlayers, true];
        } forEach allCurators;  // There should not be more than one curator
    };
}



