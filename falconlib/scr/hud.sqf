/*
	hud.sqf

	Display player names on top of the head.

	SHOW_OBJECT_LABELS will show labels for AI as well (useful for testing in local)
	SHOW_ENEMY_LABELS should be false, specially in TvT.
*/
//const
#define DEBUG_MODULE "hud"
#include "..\base.hpp"

#define HUD_DISPLAY 1
#define MAX_DISTANCE 100
#define HEAD_HEIGHT .35
#define VEHICLE_HEIGHT 2
#define COLOR(ALPHA) [230/255, 15/255, 15/255, (ALPHA)]

#define SHOW_OBJECT_LABELS false
#define SHOW_ENEMY_LABELS false

sleep 2;

DEBUG_LOG(DEBUG_MODULE, "HUD initiated.");

if (!isNil "falcon_hudEventHandler") then {
	removeMissionEventHandler ["Draw3D", falcon_hudEventHandler];
};

falcon_hudEventHandler = addMissionEventHandler ["Draw3D", {
	private ["_drawPos", "_text","_height", "_headPos", "_distance"];
	scopeName "evHandler";

	// discard null, distance and non-vehicles.
	if (isNull cursorTarget) then { breakOut "evHandler"; };
	if ((cursorTarget distance player) > MAX_DISTANCE) then { breakOut "evHandler"; };
	if (!(cursorTarget isKindOf "AllVehicles")) then { breakOut "evHandler"; };

	// select the apropriate position
	// TODO: test on water
	_headPos = (cursorTarget selectionPosition "head");
	if (_headPos isEqualTo [0,0,0]) then {
		//_drawPos = if (SurfaceIsWater (getpos cursorTarget)) then { getposASL cursorTarget; } else { getposATL cursorTarget; };
		_drawPos = visiblePosition cursorTarget;
		//_drawPos set [2, (_drawPos select 2) + (((boundingBoxReal cursorTarget) select 1) select 2)];
		_height = (_drawPos select 2) + VEHICLE_HEIGHT;
 	} else {
		_drawPos = cursorTarget modelToWorldVisual _headPos;
		_height = (_drawPos select 2) + HEAD_HEIGHT;
	};

	// get name, if vehicle get driver
	if ((cursorTarget isKindOf "Man") && (SHOW_OBJECT_LABELS || (isPlayer cursorTarget))
		&& (SHOW_ENEMY_LABELS || (side cursorTarget != side player)) ) then {
		_text = name cursorTarget;
	} else {
		if ( alive (driver cursorTarget) && (SHOW_OBJECT_LABELS || isPlayer (driver cursorTarget))) then {
			_text = name driver cursorTarget;
		} else {
			breakOut "evHandler";
		};
	};

	// draw. adjust height depending on distance.
	_distance = (player distance2D cursorTarget);
	_height = _height + _distance / 50;
	drawIcon3D [
		"", COLOR(-_distance/MAX_DISTANCE + 1),
		[_drawPos select 0, _drawPos select 1, _height],
		0, 0, 0, _text, 2, 0.04, "PuristaSemiBold"
	];

}];
