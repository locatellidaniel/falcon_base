/*
	garbageCollector.sqf


	Collects dead bodies.

	Run only on server side.

	Originally basura.sqs.
*/

#define DEBUG_MODULE "garbageCollector"
#include "..\base.hpp"

#define DELETE_MARK "falcon_garbageCollector_markedForDelete"
#define SKIP_MARK "falcon_garbageCollector_skip"

DEBUG_LOG(DEBUG_MODULE, "init");

while {true} do {
	private ["_count"];

	_count = 0;
	{
		if (not (_x getVariable [SKIP_MARK, false])) then {
			private ["_marked"];

			_marked = _x getVariable [DELETE_MARK, false];

			if (!_marked) then {
				_x setVariable [DELETE_MARK, true, false];

				[_x] spawn {
					sleep 300;
					deleteVehicle (_this select 0);
				};
				_count = _count + 1;
			};
		}

	} foreach allDeadMen;

	if (_count > 0) then {
		DEBUG_LOG(DEBUG_MODULE, "Marked " + str _count + " dead bodies.")
	};

	sleep 15;
};

true;