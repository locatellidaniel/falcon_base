/*


	Author: JohnMisil

 */

#define DEBUG_MODULE "dialogJIP"
#include "..\base.hpp"

#include "..\rsc\dialogJIP.hpp"


// global

#define MRK_JUMP "falcon_dialogJIP_mrkJump"
#define MRK_PLAYERS(INDEX) ("falcon_dialogJIP_mrkPlayers_" + str INDEX)
#define COLOR_FRIENDLY [0.403922,0.545098,0.607843,1]


private [
	// vars
	"_dlg"
];



//
// INIT
//

DEBUG_LOG(DEBUG_MODULE, "started.");

// create dialog
createDialog "FalconDialogJIP";

disableSerialization;

// get dialog control
_dlg = uiNamespace getVariable ["falcon_dialogJIP", "nota"];


(_dlg displayCtrl IDC_FALCONJIP_MAPSELECTJIP) ctrlAddEventHandler ["Draw", {
    private _ctrl = _this select 0;
    {
    	if (side _x == playerSide && alive _x) then {
    
            _ctrl drawIcon [
        		"\A3\ui_f\data\map\markers\military\triangle_CA.paa", // Custom images can also be used: getMissionPath "\myFolder\myIcon.paa"
        		COLOR_FRIENDLY,
        		_x,
        		32,
        		32,
        		getDir _x,
        		name _x,
        		2,
        		0.07,
        		"TahomaB",
        		"right"
        	];
            
    	};
    } forEach allPlayers;

}];

private _playerIndex = 0;
lbClear IDC_FALCONJIP_PLAYERS_COMBO;
{
    // get label
    private ["_text"];
    
    _text =  name _x;

    // add item
    lbAdd [IDC_FALCONJIP_PLAYERS_COMBO, _text];
    lbSetData [IDC_FALCONJIP_PLAYERS_COMBO, _forEachIndex, _text];
    
    if (_x == player) then {
        _playerIndex = _forEachIndex;
    };

} forEach (call BIS_fnc_listPlayers);

lbSetCurSel [IDC_FALCONJIP_PLAYERS_COMBO, _playerIndex];  // default first

// hide for non admins
if (isServer || (call BIS_fnc_admin) > 0) then {
    ctrlShow [IDC_FALCONJIP_PLAYERS_COMBO, true];
} else {
    ctrlShow [IDC_FALCONJIP_PLAYERS_COMBO, false];
};

//
// EVENTS
//
_dlg displayAddEventHandler ["Unload", {
	DEBUG_LOG(DEBUG_MODULE, "Unloading")

	// delete local markers
	private ["_i"];

	deleteMarkerLocal MRK_JUMP;

	closeDialog 0;
}];

(_dlg displayCtrl IDC_FALCONJIP_BTNOK) ctrlAddEventHandler ["ButtonClick", {
	private ["_pos"];

	_pos = getMarkerPos MRK_JUMP;

	if (_pos isEqualTo [0,0,0]) then {
		hintC "No has marcado un punto en el mapa!";
	} else {
        private _selectedPlayerName = lbData [IDC_FALCONJIP_PLAYERS_COMBO, lbCurSel IDC_FALCONJIP_PLAYERS_COMBO];
        DEBUG_LOGF1(DEBUG_MODULE, "Teleporting... %1", _selectedPlayerName )
        if ((name player) == _selectedPlayerName) then {
            (vehicle player) setPos _pos;
			
			// Hide teleport after use unless:
			//  Is admin
			//  player has a persistent flag 
			//  there is a global setting 
			if ( (player getVariable [VAR_PLAYER_PERSISTENT_TELEPORT, false]) || (call falcon_fnc_playerIsAdmin) || Falcon_always_teleport) then { 
				// show / do nothing
			} else {
				// hide
				[player, false] call falcon_fnc_toggleTeleportAction;
			};

    		closeDialog 0;
        } else {
            // remote teleport
            [_selectedPlayerName, _pos] remoteExec ["FALCON_fnc_teleportPlayer", 0];
        };
	};
}];

(_dlg displayCtrl IDC_FALCONJIP_BTNCANCEL) ctrlAddEventHandler ["ButtonClick", {
	closeDialog 0;
}];

(_dlg displayCtrl IDC_FALCONJIP_MAPSELECTJIP) ctrlAddEventHandler ["mouseButtonUp", {
	params ["_ctrlMap", "_btn", "_x", "_y"];

	if (_btn != 0) exitWith {};

	private ["_mapPos", "_mrk"];

	_mapPos = _ctrlMap ctrlMapScreenToWorld [_x,_y];

	deleteMarkerLocal MRK_JUMP;

	_mrk = createMarkerLocal [MRK_JUMP, _mapPos];
	_mrk setMarkerShapeLocal "ICON";
	_mrk setMarkerTypeLocal "Waypoint";
	_mrk setMarkerColorLocal "Color3_FD_F";
	_mrk setMarkerSizeLocal [1.3,1.3];
}];
