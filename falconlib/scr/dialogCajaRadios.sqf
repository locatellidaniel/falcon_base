/*


	Author: JohnMisil

 */

#define DEBUG_MODULE "dialogCajaRadios"
#include "..\base.hpp"

#include "..\rsc\dialogCajaRadios.hpp"

// global
#define ROLE 			falcon_dialogCajaRadios_role


params ["_objectBox"];

private [
	// events
	"_fnc_onClickCoger",
	"_fnc_onClickLargas",
	"_fnc_onClickCortas",
	"_fnc_onClickSalir",

	// vars
	"_dlg"
];

//
//
_fnc_onClickCoger = {
	private ["_ind", "_txt"];

	// get selected
	_ind = lbCurSel IDC_LB_RADIOS;
	_txt = lbText [IDC_LB_RADIOS, _ind];

	// add radio to equipment considering type
	if(ROLE==1) then {player addBackPack _txt};
	if(ROLE==2) then {player additem _txt}
};

//
//
_fnc_onClickLargas = {
	private ["_cosas"];

	ROLE = 1;	// set role

	lbClear IDC_LB_RADIOS;

	_cosas=["ANPRC_77","ANPRC_117F"];

	{
		lbAdd [IDC_LB_RADIOS,_x];
		lbSetPicture [IDC_LB_RADIOS,(lbSize IDC_LB_RADIOS)-1,getText (configFile >>"CfgVehicles" >>_x >> "Picture")];
	} forEach _cosas;

	lbSetCurSel [IDC_LB_RADIOS, 0];
};

//
//
_fnc_onClickCortas = {
	private ["_cosas"];

	ROLE = 2;	// set role
	lbClear IDC_LB_RADIOS;

	_cosas=["ACRE_PRC343","ACRE_PRC148","ACRE_PRC152"];

	{
		lbAdd [IDC_LB_RADIOS,_x];
		lbSetPicture [IDC_LB_RADIOS,(lbSize IDC_LB_RADIOS)-1,getText (configFile >>"CfgWeapons" >>_x >> "Picture")];
	} forEach _cosas;

	lbSetCurSel [IDC_LB_RADIOS, 0]
};

//
//
_fnc_onClickSalir = {
	closeDialog 0;
};

//
// INIT
//

DEBUG_LOG(DEBUG_MODULE, "started.");

// create dialog
createDialog "FalconDialogCajaRadios";

disableSerialization;

// get dialog control
_dlg = uiNamespace getVariable "falcon_dialogCajaRadios";

// assign event handlers
(_dlg displayCtrl IDC_BTN_SALIR) ctrlAddEventHandler ["ButtonClick", _fnc_onClickSalir];
(_dlg displayCtrl IDC_BTN_COGER) ctrlAddEventHandler ["ButtonClick", _fnc_onClickCoger];
(_dlg displayCtrl IDC_BTN_CORTAS) ctrlAddEventHandler ["ButtonClick", _fnc_onClickCortas];
(_dlg displayCtrl IDC_BTN_LARGAS) ctrlAddEventHandler ["ButtonClick", _fnc_onClickLargas];
