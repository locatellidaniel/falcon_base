/*
	fn_textToAML

	Loads a plain textfile and converts it to ARMA ML (almost html)

	Usage:

	_file the text file to load

	Returns a string with the file contents AMLized

	Example:

	["mytextfile.txt"] call fn_textToAML;

	Author: JohnMisil

 */

#include "..\..\base.hpp"

params ["_file"];

// inserts a BR after every newline char.
private _newLineReplace = {
	params ["_string"];

	private _strArray = toArray _string;
	private _ret = "";
	private _i = 0;
	private _len = count _strArray;
	private _nl = toString [10];

	while {_i < _len} do {

		private _char = toString [(_strArray select _i)];
		if (_char == _nl) then {
			_ret = _ret + _char + "<br />";
		} else {
			_ret = _ret + _char;
		};
		_i = _i + 1;
	};

	_ret;
};


[(loadFile _file), toString [13,10], "<br />"] call _newLineReplace;