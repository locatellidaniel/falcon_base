/*

	fn_intro 

	Shows Falcon Intro 

	Usage:

	_intro_title		The mission name.
	_intro_location1	The location precise: format is "PRECISE (BROAD)"
	_intro_location2	The location broader: format is "PRECISE (BROAD)"
	_intro_music		The music class to play, using playMusic
	_intro_music_offset How many seconds to skip from the start of the song.
	_intro_after_message Final message to show after title and location. can be ignored.


	Example In a "Activation" field of a Trigger:

    [
        "Operación Mandioca Rebelde",
        "Aeropuerto de Barajas",
        "Madrid"
    ] call falcon_fnc_intro;


	Author: JohnMisil


*/
#define DEBUG_MODULE "fn_intro"
#include "..\..\base.hpp"
#include "\a3\ui_f\hpp\definedikcodes.inc"

// spawn scheduled mode
if (!canSuspend) exitWith { _this spawn falcon_fnc_intro };

DEBUG_LOG(DEBUG_MODULE, "Started.");

// Globals 
#define LAYER_1 ("falcon_intro_template_layer_1" call BIS_fnc_rscLayer)
#define LAYER_2 ("falcon_intro_template_layer_2" call BIS_fnc_rscLayer)
#define FINISH_INTRO falcon_fnc_intro_skip
#define SKIP_INTRO_FLAG falcon_intro_skip_flag
#define SKIP_INTRO_KEY_HANDLE falcon_intro_skip_key_handle


// Get closest named location for default 
private _closeLocations = nearestLocations [position player, ["Name", "NameCity", "NameCityCapital", "NameLocal", "NameVillage"], 500];
private _defaultLocation = "";
if (count _closeLocations > 0) then {
    _defaultLocation = text (_closeLocations#0);
};

// Params 
params [
	["_intro_title", briefingName],
	["_intro_location1", _defaultLocation],
	["_intro_location2", worldName],
	["_intro_music", "EventTrack02b_F_Tacops"],
	["_intro_music_offset", 0],
    ["_intro_after_message", ""]
];

LAYER_1 cutText ["", "BLACK", .1];


SKIP_INTRO_FLAG = false;

// wait until game start
sleep .1;

// Silent all 
.1 fadeSpeech 0;
.1 fadeEnvironment 0;
.1 fadeRadio 0;
.1 fadeMusic 0;


//
// Auxiliary Global function for terminating the intro early
//

// exit shortcut event handler 
SKIP_INTRO_KEY_HANDLE = findDisplay 46 displayAddEventHandler ["KeyDown", {
    params ["_displayOrControl", "_key", "_shift", "_ctrl", "_alt"];
    private _return = false;
    if (_ctrl && _shift && _key isEqualTo DIK_I ) then {
        [true] call FINISH_INTRO;
        _return = true;
    };

    _return
}];

// Termination script 
FINISH_INTRO = {
    params ["_immediate"];

    if (_immediate) then {
        LAYER_1 cutFadeOut 0;
        LAYER_2 cutFadeOut 0;
        playMusic "";
        0.1 fadeSpeech 1;
        0.1 fadeEnvironment 1;
        0.1 fadeRadio 1;
        0.1 fadeMusic 1;
        SKIP_INTRO_FLAG = true;
    } else {

        [] spawn {
            10 fadeMusic 0;
            10 fadeSpeech 1;
            10 fadeEnvironment 1;
            10 fadeRadio 1;


            sleep 10;
            playMusic "";
            sleep .1;
            0.1 fadeMusic 1;
        };
    };
    

    findDisplay 46 displayRemoveEventHandler ["KeyDown", SKIP_INTRO_KEY_HANDLE];
    
};

sleep 1;

if(SKIP_INTRO_FLAG) exitWith { true };  // skip after sleep

// fade in music 
2 fadeMusic 1;
playMusic [_intro_music, _intro_music_offset];


// Clan Falcon
sleep 3;
if(SKIP_INTRO_FLAG) exitWith { true };  // skip after sleep
[parseText "<t align='center' font='PuristaBold' size='5' color='#ee0000'>CLAN FALCON</t>", [0,-.2,1,1], nil, 7.5, 0.7, 0] spawn BIS_fnc_textTiles;

sleep 1;
if(SKIP_INTRO_FLAG) exitWith { true };  // skip after sleep
LAYER_2 cutRsc ["RscTitleFalconIntroPlain", "PLAIN"];

// Arma 3
sleep 1;
if(SKIP_INTRO_FLAG) exitWith { true };  // skip after sleep
["<t align='center' font='RobotoCondensedBold' color='#ffffff' size='1.3'>ARMA 3</t>",-1,1,5,1,0] spawn BIS_fnc_dynamicText;

// Fade to black 
sleep 5;
if(SKIP_INTRO_FLAG) exitWith { true };  // skip after sleep

// Mission name and location 
sleep 3;
if(SKIP_INTRO_FLAG) exitWith { true };  // skip after sleep

LAYER_1 cutFadeOut 5/.1; // value * fadein (.1)
LAYER_2 cutFadeOut 5;


private _date = format ["%1/%2/%3", date # 2, date # 1, date # 0];
private _time = [dayTime, "HH:MM"] call BIS_fnc_timeToString;
[
    [
        [_intro_title, "<t align='right' shadow = '1' size = '1.25'>%1</t><br/>", 10],
        [format ["%1 (%2)", _intro_location1, _intro_location2], "<t align = 'right' shadow = '1' size = '.75' color='#aaaaaa'>%1</t><br/>", 5],
        [_date, "<t align = 'right' shadow = '1' size = '.75' color='#aaaaaa'>%1</t><br/>", 5],
        [_time, "<t align = 'right' shadow = '1' size = '.75' color='#aaaaaa'>%1</t>", 5],
        [_intro_after_message, "<br/><t align = 'right' shadow = '1' size = '.75' color='#eeeeee'>%1</t>", 30]
    ],
    .2,     // push it out of the safezone a bit 
    safeZoneY + safeZoneH * .65
] spawn BIS_fnc_typeText;


// Wait until all text is displayed 
sleep 10;

[false] call FINISH_INTRO;

