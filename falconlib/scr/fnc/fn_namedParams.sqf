/*
	fn_namedParams
    
    Use in conjuntion with params in order to pass named params.
    
    Use only for user-facing one-time initialization functions since its not very efficient and 
     its only for making it easier to pass parameters from user side.
    
    (example)
    private _args = [createHashMapFromArray [
        ["_param1", [objNull, true]],
        ["_option2", [20]],
        ["_option3", [5]]
    ], _this] call falcon_fnc_namedParams;
    
    if (!(_args isEqualType createHashMap )) exitWith {
        // incorrect params 
    };
        
    // assign the parameters to variables 
    (values _args) params (keys _args);
    
    // now it is a private variable!
    hint _option2 - _option3;
    
    (example end)

	Author: JohnMisil
 */

#include "..\..\base.hpp"


params [["_defaults", createHashMap, [createHashMap]], ["_arguments", []]];

private _argumentsHash = createHashMapFromArray _arguments;
private _result = createHashMap;

scopeName "main";

{
    private _key = _x;
    private _default = _defaults get _key;
    
    private ["_isMandatory", "_defaultValue", "_value"];
    
    
    _defaultValue = _default select 0;  // The default value 
    _isMandatory = if (count _default > 1) then { _default select 1 } else { false };   // whether the var is mandatory 
    
    // Check if defined in arguments 
    if (_key in _argumentsHash) then {
        _value = _argumentsHash get _key;
    };
    
    // Also check for missing "_"
    private _keyAlternative = _key select [1];
    if (_keyAlternative in _argumentsHash) then {
        _value = _argumentsHash get _keyAlternative;
    };
    
    // Show error if no value and it is a mandatory param.    
    if ((isNil "_value") and _isMandatory) then {
        ["ERROR: Falta parámetro: %1", _key] call BIS_fnc_error; 
        _result = false;
        breakTo "main"; // exit foreach
    } else {
        // Assign default 
        if (isNil "_value") then {
            _value = _defaultValue;
        };
    };
    
    // Show error if type is wrong
    if (!(_value isEqualType _defaultValue)) then {
        ["ERROR: Tipo de parámetro incorrecto: %1. Valor: %2", _key , _value] call BIS_fnc_error; 
        _result = false;
        breakTo "main"; // exit foreach
    };
    
    
    _result set [_key, _value];
} forEach (keys _defaults);


_result
