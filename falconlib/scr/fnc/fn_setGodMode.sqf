/*
	fn_setGodMode

	enables/disables god mode
	_target	"player" or "global" (scope)
	_enabled true/false
*/
#define DEBUG_MODULE "fn_setGodMode"
#include "..\..\base.hpp"

params [["_target", "player"], "_enabled"];

#define VAR_GODMODE_PLAYER "falcon_var_playerGodMode"
#define VAR_GODMODE_GLOBAL "falcon_var_globalGodMode"


if (!isNil "_enabled") then {
	// make permanent setting for this unit
	if (_target == "player") then {
		player setVariable [VAR_GODMODE_PLAYER, _enabled];
	} else { // make permanent setting for all
		missionNamespace setVariable [VAR_GODMODE_GLOBAL, _enabled];
		publicVariable VAR_GODMODE_GLOBAL;
	};
};

// based on the new settings set god mode according to scope
// MissionParam > PLAYER scope > GLOBAL scope

_resolveEnabled = missionNamespace getVariable [VAR_GODMODE_GLOBAL, false];

_resolveEnabled = player getVariable [VAR_GODMODE_PLAYER, _resolveEnabled];

if ((["FalconGodMode", 0] call falcon_fnc_resolveScenarioParam) == 1) then {
	_resolveEnabled = true;

	// disable rs_med if god mode!
	//rs_med=0;
} else {
	if (isNil "rs_med") then { rs_med=1; };
};


if (_resolveEnabled) then {
	DEBUG_LOG(DEBUG_MODULE, "enabling god mode")
	player setVariable ["dios", 1, true];
	player allowDamage false;
} else {
	DEBUG_LOG(DEBUG_MODULE, "disabling god mode")
	player setVariable ["dios", nil, true];
	player allowDamage true;
};

true;