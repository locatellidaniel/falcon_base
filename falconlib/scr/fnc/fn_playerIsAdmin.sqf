/*
	fn_playerIsAdmin

	Usage:

	( call falcon_fnc_playerIsAdmin)

	Author: JohnMisil

 */

#include "..\..\base.hpp"

// dedicated server
if (isNull player) exitWith { false };

// singleplayer/self-hosted or admin 
if (isServer || (call BIS_fnc_admin) > 0) exitWith { true };

false