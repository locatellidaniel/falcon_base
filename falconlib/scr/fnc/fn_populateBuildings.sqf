/*
	fn_populateBuildings

	Fill buildings in area randomly. Returns an array of the created units.

	Usage:

	_units = [_position, _radius, _classes, _min, _max, _initScript];

	_position 	(Position) 	center position to look around for buildings.
	_radius 	(Number)	distance from center position.
	_classes	(Array)		array of unit classnames as strings.
	_min		(Number)	min number of units to create.
	_max		(Number)	max number of units to create.
	_initScript	(Code)		Optional. code to execute after each unit is created. Use _this to get a reference to the unit.
	_side		(Side)		Optional. Default EAST.

	Example:

	[getPos player, 100, ["Rifleman"], 1, 4, { _this setBehaviour "SAFE"; }] call falcon_fnc_populateBuildings;

	Notes: execute on Server side only.

	Author: JohnMisil

 */
#define DEBUG_MODULE "fn_populateBuildings"
#include "..\..\base.hpp"

// PARAMS
params ["_pos","_radius", "_classes", "_min", "_max", "_initScript",["_side", east]];

// LOCALS
private ["_unitCount", "_buildings", "_positions", "_createdUnits"];

_createdUnits = [];
_unitCount = floor (random [_min, (_min + _max / 2), _max]);


DEBUG_LOGF2(DEBUG_MODULE, "populating %1 -> %2", _pos, _radius);
// Locate buildings
_buildings = _pos nearObjects ["House", _radius];
if (count _buildings == 0) exitWith { DEBUG_LOG(DEBUG_MODULE, "no buildings found."); []; };

DEBUG_LOGF1(DEBUG_MODULE, "Found %1 buildings", count _buildings);

// Positions, grupped by buildings.
_positions = [];
{
	private ["_bp"];
	_bp = [_x] call BIS_fnc_buildingPositions;

	if (count _bp > 0) then {
		_positions append _bp;
	};
} forEach _buildings;
if (count _positions == 0) exitWith { hint "No positions found"; []; };


// setup units

DEBUG_LOGF2(DEBUG_MODULE, "creating %1 units. %2 positions.", _unitCount, count _positions);
while { _unitCount > 0 } do {
	private ["_b", "_bi", "_p1","_p2", "_g", "_u"];
	_g = createGroup _side;	// one group per unit
	//_bi = (_positions call BIS_fnc_randomIndex);	// building index
	//_b = _positions select _bi;						// positions in building

	_p1 = _positions deleteAt (_positions call BIS_fnc_randomIndex);	// remove position from array so there are no 2 units in the same pos.

	// create a unit of random _classes
	_u = _g createUnit [(_classes call BIS_fnc_selectRandom), _p1, [], 0, "CAN_COLLIDE"];
	[_u] joinSilent _g;	// workaround createUnit wrong side bug.
	_createdUnits pushBack _u;

	DEBUG_LOGF1(DEBUG_MODULE, "created unit on %1", _p1);

	// 50% chance of adding a random two-point patrol
	if ((random 10) >= 5 && count _positions > 0) then {
		_p2 = _positions deleteAt (_positions call BIS_fnc_randomIndex);
		(_g addWaypoint [_p2,0]) setWaypointTimeout [0,10,60];
		(_g addWaypoint [_p1,0]) setWaypointTimeout [0,10,60];
		(_g addWaypoint [_p2,0]) setWaypointType "CYCLE";
	};

	// no more positions in building.
	//if (count _positions == 0) then { _positions deleteAt _bi; };

	// exec init script
	if (!isNil "_initScript") then {
		_u call _initScript;
	};


	_unitCount = _unitCount - 1;
};

_createdUnits;