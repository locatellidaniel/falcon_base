/*
	fn_spectatorMode

	enables/disables spectator mode for player

	_enabled true/false
*/
#define DEBUG_MODULE "fn_spectatorMode"
#include "..\..\base.hpp"

params [["_enabled", true]];

#define SPECTATOR_KEY_EH falcon_eventhandler_spectator_keydown


if (_enabled) then {
	DEBUG_LOG(DEBUG_MODULE, "Enabling spectator mode");
	disableSerialization;

	["Initialize", [player]] call BIS_fnc_EGSpectator;

	// wait until spectator display is created
	waitUntil { !isNull (["GetDisplay"] call BIS_fnc_EGSpectator); };

	// hook escape key to exit spectator mode
	private ["_spectatorDisplay"];
	_spectatorDisplay = ["GetDisplay"] call BIS_fnc_EGSpectator;

	SPECTATOR_KEY_EH = _spectatorDisplay displayAddEventHandler ["keyDown", {
		params ["_display", "_dikCode", "_shift", "_ctrl", "_alt"];

		// ESCAPE
		if (_dikCode == 0x01) then {
			[false] call falcon_fnc_spectatorMode;
			true;	// do not execute game action (show main menu)
		} else {
			false;
		};
	}];

	hint "Presiona [ESC] para salir del modo espectador.";

	// disable acre spectator mode (death channel)
	[false] call acre_api_fnc_setSpectator;

} else {
	DEBUG_LOG(DEBUG_MODULE, "Disabling spectator mode");
	disableSerialization;

	private ["_spectatorDisplay"];
	_spectatorDisplay = ["GetDisplay"] call BIS_fnc_EGSpectator;

	// remove keyup handler and terminate
	if (!isNull _spectatorDisplay) then {

		DEBUG_LOG(DEBUG_MODULE, "removing hook");
		_spectatorDisplay displayRemoveEventHandler ["keyDown", SPECTATOR_KEY_EH];

		DEBUG_LOG(DEBUG_MODULE, "Terminating");
		["Terminate", [player]] call BIS_fnc_EGSpectator;
	};
};


true;
