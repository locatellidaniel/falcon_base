/*
	fn_nearestPlayer

	Find the closest player to the unit

	Usage:

	_unit 	The unit.


	Example In a "Activation" field of a Trigger:

	myUnit call falcon_fnc_nearestPlayer;


	Author: JohnMisil

 */

#define DEBUG_MODULE "fn_nearestPlayer"
#include "..\..\base.hpp"

params ["_unit"];

_players = allPlayers apply { [_unit distanceSqr _x, _x] };
_players sort true;

(_players#0)#1