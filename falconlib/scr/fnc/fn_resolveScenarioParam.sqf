/*
	fn_resolveScenarioDefaultParam

	Gets a scenario param considering the chosen value in MP parameters prioritized:

	1- Chosen value (if different from -1:"Scenario")
	2- Scenario default (as specified in preInit scenario script) if -1 : "Scenario" is chosen.
	3- If for whatever reason, no value has been specified, then the passed default is used

*/
#define SCENARIO_PARAM "falcon_scenario_param_"

params["_param", "_default"];
private ["_value"];

_value = [_param, _default] call BIS_fnc_getParamValue;

if (_value == 998) then {
	_value = missionNamespace getVariable [SCENARIO_PARAM  + _param, _default];
};
_value;