/*
	fn_toggleInvisibleAction

	Add Invisible menu action to player

	Usage:

	_unit		unit to add, must be local so its usually player
	_enable		true/false to hide or show the action. defaults to true.

	Example:

	this call fn_toggleInvisibleAction;


	Author: JohnMisil

 */

#include "..\..\base.hpp"

// GLOBAL object's variable
#define INVISIBLE_ACTION "falcon_menuaction_switchInvisible"

params ["_unit", ["_enable", true]];


_unit removeAction (_unit getVariable INVISIBLE_ACTION);

if (_enable) then {
	private ["_actionMenuText"];

	_actionMenuText = if (isObjectHidden _unit) then { '<t color="#FF2222">Invisible OFF</t>'; } else { '<t color="#22FF22">Invisible ON</t>'; };

	_unit setVariable [INVISIBLE_ACTION, _unit addAction [
		_actionMenuText,
		{
			private _unit = _this select 3;

			if (isObjectHidden _unit) then {
				[_unit, false] remoteExec ["FALCON_fnc_server_setInvisible", 2];
				hint "Ahora eres visible!";
			} else {
				[_unit, true] remoteExec ["FALCON_fnc_server_setInvisible", 2];
				hint "Ahora eres invisible!";
			};

		},
		_unit,
		6,
		false,
		true
	]];

};