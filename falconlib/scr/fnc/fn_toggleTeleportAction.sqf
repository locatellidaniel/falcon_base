/*
	fn_toggleTeleportAction

	Add JIP menu action to unit/object

	Usage:

	_unit		the player or object
	_enabled	true | false : whether to add the action or remove it.

	Example:

	this call fn_toggleTeleportAction;


	Author: JohnMisil

 */

#include "..\..\base.hpp"

// GLOBAL object's variable
#define JIP_ACTION "falcon_menuaction_doTeleport"

params ["_unit", "_enabled"];


// Make sure action appears only once
_unit removeAction (_unit getVariable JIP_ACTION);

if (_enabled) then {
	_unit setVariable [JIP_ACTION, _unit addAction [
		'<t color="#FF2222">JIP</t>',
		{
			[] execVM LIB_SCRIPT("dialogJIP.sqf");
		},
		nil,
		6,
		false,
		false
	]];
};