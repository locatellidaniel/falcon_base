/*
	fn_initializeBackpack

	Initializes player's Backpack according to RS

	Usage:

	_unit		the ammobox

	Example:

	this call fnc_falcon_initRadioBox;

	Notes: executed by dialogCajaUniformes.sqf; original functionality: vaciar.sqs

	Author: JohnMisil

 */

#include "..\..\base.hpp"



// TODO: do we need to define these globals?

cant_ven_b=0;
cant_med_b=0;
cant_mor_b=0;
cant_epi_b=0;
cant_ap_b=0;
cant_bp_b=0;
cant_ab_b=0;
cant_tor_b=0;
cant_vene_b=0;
cant_ciru_b=0;


cant_ven_v=0;
cant_med_v=0;
cant_mor_v=0;
cant_epi_v=0;
cant_ap_v=0;
cant_bp_v=0;
cant_ab_v=0;
cant_tor_v=0;
cant_vene_v=0;
cant_ciru_v=0;

cant_ven_u=0;
cant_med_u=0;
cant_mor_u=0;
cant_epi_u=0;
cant_ap_u=0;
cant_bp_u=0;
cant_ab_u=0;
cant_tor_u=0;
cant_vene_u=0;
cant_ciru_u=0;

// empty and add basic med stuff
if((BackPack player)!="Mochila_med") then {

	clearAllItemsFromBackpack player;
/*
	//(backpackContainer player) addMagazineCargoGlobal ["sc_bendae",10];
	//(backpackContainer player) addMagazineCargoGlobal ["sc_torniquete",10];
	(backpackContainer player) addMagazineCargoGlobal ["sc_morfina",10];
	(backpackContainer player) addMagazineCargoGlobal ["sc_epinefrina",10];
	(backpackContainer player) addMagazineCargoGlobal ["sc_medikit",2];
	(backpackContainer player) addMagazineCargoGlobal ["sc_cirugia",2];*/
};

true;
