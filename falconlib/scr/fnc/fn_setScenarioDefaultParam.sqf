/*
	fn_setScenarioDefaultParam

	Sets a scenario param default for this mission.
*/
#define SCENARIO_PARAM "falcon_scenario_param_"
params["_param", "_value"];


missionNamespace setVariable [SCENARIO_PARAM  + _param, _value];

true;