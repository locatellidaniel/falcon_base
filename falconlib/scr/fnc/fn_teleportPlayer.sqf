/*
	fn_teleportPlayer

	Usage:

	_pos   the position to teleport

	Example:

	_pos call fn_teleportPlayer;

	Author: JohnMisil

 */

#include "..\..\base.hpp"


params ["_name", "_pos"];

if ((name player) == _name) then {
    hint format ["Has sido teletransportado!"];
    (vehicle player) setPos _pos;
};
