#define DEBUG_MODULE "missionControlStart"
#include "..\..\base.hpp"

if (!isServer) exitWith { true };

if (!canSuspend) exitWith { _this spawn falcon_fnc_hostage_embarkClient };

#define STR_VAR_SPAWNS "falcon_var_missionControl_spawns"

DEBUG_LOG(DEBUG_MODULE, "Started");

// for now we just return admins
private _fnc_get_controllers = {
	(call BIS_fnc_listPlayers) select { (admin owner _x) > 0 };
};

while { true } do {
	private _spawns = missionNamespace getVariable [STR_VAR_SPAWNS, createHashMap];

	private _spawnsUpdate = [];

	{
		private _sp = createHashMapFromArray [
			["position", position _x], 
			["activated", _x getVariable ["activated", false]]
		];

		_spawnsUpdate pushBack _sp;
	} forEach _spawns;
	

	// publish only to the controllers 
	missionNamespace setVariable ["falcon_var_missionControl_spawnsUpdate", _spawnsUpdate, call _fnc_get_controllers];
	
	DEBUG_LOG(DEBUG_MODULE, "Updated");
	sleep 5;
};