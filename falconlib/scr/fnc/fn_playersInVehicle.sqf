/*
	fn_playersInVehicle

	Checks if there is at least one alive player inside the vehicle

	Usage:

	_vehicle 	the vehicle to check


	Author: JohnMisil

 */

#define DEBUG_MODULE "fn_playersInVehicle"
#include "..\..\base.hpp"

params ["_vehicle"];

private _found = allPlayers findIf { (alive _x) && (_x in _vehicle) };

(_found > -1)