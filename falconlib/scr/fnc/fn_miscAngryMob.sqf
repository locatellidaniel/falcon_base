/*
	fn_miscAngryMob

	Makes a group behave like an angry mob when a trigger (SIDE activated) is active and 
	 presence is detected there.

	Usage:

	_mobGroup		group that will start the behaviour.
	_trigger		the trigger. It must have: an area, it must be of type PRESENT.
	_sound			a sound class to be played using say3D. If empty, no sound will be played.
	_soundDistance	the distance that the sound can be heard. Check say3D docs.


	Example In a "Activation" field of a Trigger:

	[myGroup, thisTrigger] call falcon_fnc_miscAngryMob;


	Author: JohnMisil

 */
#define DEBUG_MODULE "fn_miscAngryMob"
#include "..\..\base.hpp"

if (!canSuspend) exitWith { _this spawn falcon_fnc_miscAngryMob };

params ["_mobGroup", "_trigger", ["_sound", ""], ["_soundDistance", 50]];

DEBUG_LOG(DEBUG_MODULE, "Started.");

private _THROWABLE_CLASSES = [
	"Land_Mustard_01_F","Land_Pumpkin_01_F", "Land_Ketchup_01_F", 
	"Land_BakedBeans_F", "Land_Canteen_F", "Land_Orange_01_F", "Land_Can_Rusty_F",
	"Land_Wrench_F"
];
private _garbage = [];

private _mob = (units _mobGroup);
while { (count (list _trigger) > 0) && (count _mob > 0) } do {

	// select someone from the mob	
	_unit = selectRandom _mob;

	// Yell
	_unit setRandomLip true;
	if (_sound != "") then {
		[_unit, [_sound, _soundDistance, 1, true, 0 ]] remoteExec ["say3D", 0];
	};

	// Find someone to throw sth at 
	private _target = selectRandom (list _trigger);

	// look at the target
	_unit setDir ([_unit, _target] call BIS_fnc_dirTo);

	

	// throw movement
	[_unit, "AwopPercMstpSgthWnonDnon_start"] remoteExec ['switchMove', 0];
	[_unit, "AwopPercMstpSgthWnonDnon_start"] remoteExec ['playmovenow', 0];
	sleep 1.0;

	// position of the object (a bit on top of the unit)
	private _origin = getPos _unit; 
	_origin set [2, (_origin select 2) +1.5]; 

	// throw parameters
	private _direction = vectorNormalized ((vectorDir _unit) vectorAdd [0,0,.2]);
	private _class = selectRandom _THROWABLE_CLASSES;
	private _throwable = createVehicle [_class, _origin, [], 0, "CAN_COLLIDE"]; 
	private _force = (10.0 + random 5.0) * getMass _throwable;
	
	// throw
	_throwable addForce [_direction vectorMultiply _force, [0,0, .1 - random .2]];


	// wait a little bit 
	sleep 3 + random 3;
	
	// Cleanup 
	// Leave only the last five items 
	_garbage pushBack _throwable;
	if (count _garbage > 5) then {
		deleteVehicle (_garbage deleteAt 0); // remove from array and scenario
	};

	// update mob 
	_mob = _mob select { (alive _x) && (_x inArea _trigger) };
};

// remove all remaining garbage 

{ deleteVehicle _x; } forEach _garbage;

DEBUG_LOG(DEBUG_MODULE, "mob finished.")


