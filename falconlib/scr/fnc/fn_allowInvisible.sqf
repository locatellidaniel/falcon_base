/*
	fn_persistentInvisibleAction

	Allow a unit to teleport indefintely using the menu action

	Usage:

	_unit		the player or object

	Example:

	this call fn_persistentInvisibleAction;


	Author: JohnMisil

 */

#include "..\..\base.hpp"

params ["_unit"];

_unit setVariable [VAR_PLAYER_PERSISTENT_INVISIBLE, true];