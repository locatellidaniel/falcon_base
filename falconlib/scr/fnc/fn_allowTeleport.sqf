/*
	fn_persistentTeleportAction

	Allow a unit to teleport indefintely using the menu action

	Usage:

	_unit		the player or object

	Example:

	this call fn_persistentTeleportAction;


	Author: JohnMisil

 */

#include "..\..\base.hpp"

params ["_unit"];

_unit setVariable [VAR_PLAYER_PERSISTENT_TELEPORT, true];