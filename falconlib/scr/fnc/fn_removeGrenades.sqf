/*
	fn_textToAML

	Loads a plain textfile and converts it to ARMA ML (almost html)

	Usage:

	_file the text file to load

	Returns a string with the file contents AMLized

	Example:

	["mytextfile.txt"] call fn_textToAML;

	Author: JohnMisil

 */

#include "..\..\base.hpp"

params ["_unit"];

#define VAR_GRENADE_MAGAZINES "falcon_var_grenadeMagazines"

// Get non-smoke hand grenades 
// Calculate only once and then store globally
private _grenades = missionNamespace getVariable [VAR_GRENADE_MAGAZINES, []];

if (count _grenades < 1) then {
    {
        if ((toLower _x) find "smoke" != -1) then { continue };
        _grenades append getArray(configFile >> "CfgWeapons" >> "Throw" >> _x >> "magazines");
    } forEach getArray(configFile >> "CfgWeapons" >> "Throw" >> "muzzles");
    
    missionNamespace setVariable [VAR_GRENADE_MAGAZINES, _grenades];
};

{
    _unit removeMagazines _x;
} forEach _grenades;
