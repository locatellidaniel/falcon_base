/*
	fn_enableSpectatorAction

	Add Invisible menu action to player

	Usage:

	_enable		true/false to hide or show the action. defaults to true.

	Example:

	this call fn_enableSpectatorAction;


	Author: JohnMisil

 */

#include "..\..\base.hpp"

// GLOBAL object's variable
#define SPECTATOR_ACTION "falcon_menuaction_switchSpectator"

params [["_enable", true]];


player removeAction (player getVariable SPECTATOR_ACTION);

if (_enable) then {
	private ["_actionMenuText"];

	_actionMenuText = '<t color="#F0B52B">Spectator</t>';

	player setVariable [SPECTATOR_ACTION, player addAction [
		_actionMenuText,
		{

			[true] call falcon_fnc_spectatorMode;
		},
		[],
		6,
		false,
		true
	]];

};