#define DEBUG_MODULE "missionControlAdd"
#include "..\..\base.hpp"

params ["_type", "_instance"];


DEBUG_LOG(DEBUG_MODULE, "Added");

private _spawns = missionNamespace getVariable ["falcon_var_missionControl_spawns", []];

_spawns pushBack _instance;

missionNamespace setVariable ["falcon_var_missionControl_spawns", _spawns];
