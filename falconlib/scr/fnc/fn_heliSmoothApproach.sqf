/*
	fn_heliSmoothApproach

	Smooths helicopter approach to the next wp
    
    [group, "start"] call falcon_fnc_heliSmoothApproach;
    [group, "stop"] call falcon_fnc_heliSmoothApproach;
    
*/

#define DEBUG_MODULE "fn_heliSmoothApproach"
#include "..\..\base.hpp"

params["_heli"];

if (!isServer) exitWith { };

// spawn scheduled mode
if (!canSuspend) exitWith { _this spawn falcon_fnc_heliSmoothApproach; };


// Target Speed in kmh
#define TARGET_SPEED (10 / 3.6)

// interval in s
#define INTERVAL 3

// max reduction in one interval m/s
#define MAX_REDUCTION (8 / 3.6)

    
DEBUG_LOG(DEBUG_MODULE, "Start smoothing");
private _group = group (driver _heli);
private _nextWaypoint = waypointPosition ( (waypoints _group) select (currentWaypoint _group));
private _startSpeed = speed _heli;


private _speed = (speed _heli) / 3.6;               // speed in m/s
private _distance = _heli distance _nextWaypoint;   // distance in m

while { _speed > TARGET_SPEED } do {
    private _eta = _distance / _speed;                  // eta in s

    // how much to reduce speed in the current interval (m/s)
    private _reduction = ((_speed - TARGET_SPEED) / (_eta / INTERVAL)) min MAX_REDUCTION;
    
    // limit km/h
    _heli limitSpeed ((_speed - _reduction) *3.6);

    DEBUG_LOGF1(DEBUG_MODULE, "Reduced by %1", _reduction);
    sleep INTERVAL;

    // update speed / distance
    _speed = (speed _heli) / 3.6;
    _distance = _heli distance _nextWaypoint;
};

// reset speed limit
_heli limitSpeed 9999;
DEBUG_LOG(DEBUG_MODULE, "Complete!");




true;
