/*
	fn_initRadioBox

	Initializes Falcon's radio ammo Box

	Usage:

	_unit		the ammobox

	Example:

	this call falcon_fnc_initRadioBox;

	Notes: execute on every client (put it on editor's init field)

	Author: JohnMisil

 */

#include "..\..\base.hpp"

params ["_unit"];


_unit addAction [
	'<t color="#FFF200">Radios</t>',
	{ [] execVM LIB_SCRIPT("dialogCajaRadios.sqf"); },
	"",
	6,
	true,
	false,
	"",
	"((_target distance _this) < 3)"
];
