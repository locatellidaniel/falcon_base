/*
	fn_getRandomCoordinates

	Gets a random coordinates (z=0) inside trigger area.
	It doesn't work correctly with elliptical triggers. Only non-rotated rects and circles.

	Usage:

	_trigger		trigger

	Example:

	Author: JohnMisil

 */

params ["_trigger"];
private ["_coord", "_a", "_b", "_circle"];
_a = (triggerArea _trigger) select 0;
_b = (triggerArea _trigger) select 1;
_circle = (triggerArea _trigger) select 3;

diag_log "randomcoords bro!";

if (_circle) then {
	private ["_t1", "_t2", "_swap"];

	// two random numbers where _t1 < _t2
	_t1 = random 1;
	_t2 = random 1;
	if (_t2 < _t1) then { _swap = _t1; _t2 = _t1; _t1 = _swap; };

	// random position inside the circle
	_coord = [
		_t2 * _a * cos( 2 * pi * _t1 / _t2),
		_t1 * _a * sin( 2 * pi * _t1 / _t2),
		0
	];

	// translation to trigger center
	_coord = _coord vectorAdd position _trigger;
} else {
	// random position inside the rect
	_coord = [
		random (2 * _a) - _a,
		random (2 * _b) - _b,
		0
	];

	// translation to trigger center
	_coord = _coord vectorAdd position _trigger;
};

_coord;