/*
	fn_followNearestPlayer

	Makes a group find and follow the nearest player. Useful for vehicle persecution, or attack.

	Usage:

	_groupOrUnit		the group that will follow the closest player
	_updateFrequency 	How often to update the player's position (default to 20) in seconds.
	_maxDistance	 	Max distance to abandon the task. (default to 2000) in meters. Useful so Ai does not follow players back to base.


	Example In a "Activation" field of a Trigger:

	[myGroup] call falcon_fnc_followNearestPlayer;


	Author: JohnMisil

 */
#define DEBUG_MODULE "fn_followNearestPlayer"
#include "..\..\base.hpp"

if (!isServer) exitWith {};

if (!canSuspend) exitWith { _this spawn falcon_fnc_followNearestPlayer };

params ["_groupOrUnit", ["_frequency", 20], ["_maxDistance", 2000]];

// initial pause
sleep 1;

private _leader = leader _groupOrUnit;
private _group = group _leader;
private _target = _leader call falcon_fnc_nearestPlayer;

DEBUG_LOG(DEBUG_MODULE, "Start follow");
while { (alive _leader) && (_leader distance2d _target < _maxDistance)} do {
	_group move (position _target);
	_group setSpeedMode "FULL";
		
	sleep _frequency;
	_target = _leader call falcon_fnc_nearestPlayer;
};

DEBUG_LOG(DEBUG_MODULE, "Follow end.");