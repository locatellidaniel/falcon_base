/*
	fn_copyCargo

	Copies the cargo of a box to another
    
    [src, dest] call falcon_fnc_copyCargo 
    
*/
params["_src", "_dest"];
private ["_cargo"];

_cargo = getWeaponCargo _src;
{  
    private _count = (_cargo select 1) select _forEachIndex;
    _dest addWeaponCargoGlobal [_x, _count];
} forEach (_cargo select 0);

_cargo = getItemCargo _src;
{  
    private _count = (_cargo select 1) select _forEachIndex;
    _dest addItemCargoGlobal [_x, _count];
} forEach (_cargo select 0);

_cargo = getMagazineCargo _src;
{  
    private _count = (_cargo select 1) select _forEachIndex;
    _dest addMagazineCargoGlobal [_x, _count];
} forEach (_cargo select 0);

true;
