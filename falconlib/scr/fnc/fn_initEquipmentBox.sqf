/*
	fn_initEquipmentBox

	Initializes Falcon's Equipment ammo Box

	Usage:

	_unit		the ammobox

	Example:

	this call fn_initEquipmentBox;

	Notes: execute on every client (put it on editor's init field)

	Author: JohnMisil

 */

#include "..\..\base.hpp"


params ["_unit"];


_unit addAction [
	'<t color="#FFF200">Uniformes</t>',
	{ [] execVM LIB_SCRIPT("dialogCajaUniformes.sqf"); },
	"",
	6,
	true,
	false,
	"",
	"((_target distance _this) < 3)"
];


