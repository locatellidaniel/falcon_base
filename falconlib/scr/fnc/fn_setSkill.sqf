/*
	fn_setSkill

	Sets the skill of an unit or a group.

	Usage:

	_unit		Unit or group object.
	_level		String. One of: "easy", "medium", "hard" or 1,2,3
	_multiplier	Number. Defaults to 1. Multiplies each skill for this number. use it to slightly change some units abilities.

	Example:

	[this, "medium"] call falcon_fnc_setSkill;

	Notes: execute on Server side only: where AI unit is local.

	Author: JohnMisil

 */

#define DEBUG_MODULE "fn_setSkill"
#include "..\..\base.hpp"

params ["_unitOrGroup", ["_level", 0], ["_multiplier", 1]];
private ["_units"];

if (typeName _unitOrGroup == "GROUP") then {
	_units = units _unitOrGroup;
} else {
	_units = [_unitOrGroup];
};

if (_level == 0) then {
	_level = ["FalconDifficultyLevel", 1] call falcon_fnc_resolveScenarioParam;
};


{

	switch (_level) do {
		case 1;
		case "easy": {
			_x setSkill ["general", 0.2 * _multiplier];
			_x setSkill ["aimingspeed", 0.05 * _multiplier];
			_x setSkill ["aimingaccuracy", 0.01 * _multiplier];
			_x setSkill ["aimingshake", 0.03 * _multiplier];
			_x setSkill ["spotdistance", 0.2 * _multiplier];
			_x setSkill ["spottime", 0.1 * _multiplier];
			_x setSkill ["commanding", 0.2 * _multiplier];
			_x setSkill ["courage", 0.4 * _multiplier];
		};
		case 2;
		case "medium": {
			_x setSkill ["general", 0.2 * _multiplier];
			_x setSkill ["aimingspeed", 0.05 * _multiplier];
			_x setSkill ["aimingaccuracy", 0.03 * _multiplier];
			_x setSkill ["aimingshake", 0.05 * _multiplier];
			_x setSkill ["spotdistance", 0.4 * _multiplier];
			_x setSkill ["spottime", 0.1 * _multiplier];
			_x setSkill ["commanding", 0.3 * _multiplier];
			_x setSkill ["courage", 0.6 * _multiplier];
		};
		case 3;
		case "hard": {
			_x setSkill ["general", 0.2 * _multiplier];
			_x setSkill ["aimingspeed", 0.09 * _multiplier];
			_x setSkill ["aimingaccuracy", 0.08 * _multiplier];
			_x setSkill ["aimingshake", 0.08 * _multiplier];
			_x setSkill ["spotdistance", 0.4 * _multiplier];
			_x setSkill ["spottime", 0.12 * _multiplier];
			_x setSkill ["commanding", 0.4 * _multiplier];
			_x setSkill ["courage", 0.8 * _multiplier];
		};
	};
} forEach (_units);

_unitOrGroup;