/**


*/

if (!isServer) exitWith {};

params ["_player", "_enabled"];

if (isNil "_player" || isNil "_enabled") exitWith {};

_player hideObjectGlobal _enabled;

// show the menu action for the player again
[_player, true] remoteExec ["FALCON_fnc_toggleInvisibleAction", _player];