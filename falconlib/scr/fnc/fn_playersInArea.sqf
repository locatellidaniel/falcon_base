/*
	fn_playersInArea

	Checks if there is a cluster of players inside the trigger area.

	It is not very precise, but memoized and thus fast.

	Usage:

	_trigger 	The trigger or entity as defined in inArea function


	Author: JohnMisil

 */

#define DEBUG_MODULE "fn_playersInArea"
#include "..\..\base.hpp"

// Check if there are players in the triggerArea    
private _players_in_area = false;
{
	if (_x inArea _trigger) then {
		_players_in_area = true;
		break;
	};
} forEach ([] call falcon_fnc_aiSpawn_trackPlayerPositions);

_players_in_area