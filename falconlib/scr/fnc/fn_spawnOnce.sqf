/*
	fn_spawnOnce

	To add randomization call this function in the init of several units to
	 delete all of them but one at mission start.

	The unit is selected randomly between all units initialized with the same name.

	A global name for the unit is assigned, so there should not be any unit with the same name.


	Usage:

	[this, 'objetivo1'] call fn_spawnOnce 


	Author: JohnMisil

 */

#define DEBUG_MODULE "spawnOnce"
#include "..\..\base.hpp"

#define SO_UNITS_VAR "falcon_var_spawnOnce"

params ["_unit", "_name"];


if (! isNil _name) exitWith {
    ["WARN: Falcon SpawnOnce. Ya existe un objeto con el nombre: %1", _name] call BIS_fnc_error; 
};

private _so_units = missionNamespace getVariable [SO_UNITS_VAR, createHashMap];

if (! (_name in _so_units)) then {
	_so_units set [_name, [_unit]];
} else {
	private _list = _so_units get _name;
	_list pushBack _unit;
	_so_units set [_name, _list];
};

missionNamespace setVariable [SO_UNITS_VAR, _so_units];

DEBUG_LOGF1(DEBUG_MODULE, "SpawnOnce.", _name);

true