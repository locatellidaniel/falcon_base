/*
	fn_debugMessage

	Prints a debug message according to global settings.
	Broadcasts to all clients
*/
params["_module", "_text"];
private ["_msg"];

if ( !Falcon_debug_enabled ) exitWith { true };

_msg = format ["[%1][%2] %3", if(isDedicated) then {"SERVER"} else {profileName}, _module, _text];

// only publish SERVER debug messages
if (isDedicated) then {
	_msg remoteExec ["systemChat", 0];
	_msg remoteExec ["diag_log", 0];
} else {
	systemChat _msg;
	diag_log (_msg);
};



true;