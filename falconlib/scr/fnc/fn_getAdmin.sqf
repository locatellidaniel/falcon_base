/*
	fn_getAdmin

	gets admin player object


	Usage:

	( call falcon_fnc_getAdmin )


	Author: JohnMisil

 */

#include "..\..\base.hpp"

// SP / self-hosted
if (isServer && not (isNull player)) exitWith { player };

// MP dedicated
private _adminPlayers = allPlayers select {admin (owner _x) > 0};
if ((count _adminPlayers) > 0) exitWith { _adminPlayers#0 };

objNull