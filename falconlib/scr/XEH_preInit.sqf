[
    "Falcon_ViewDistance", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "SLIDER", // setting type
    "View Distance", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Falcon",
    [200, 15000, 2000, 0], // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {  
        params ["_value"];
        setViewDistance _value;
    } // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

[
    "Falcon_aiSpawn_maxSpawnedUnits", // Internal setting name, should always contain a tag! This will be the global variable which takes the value of the setting.
    "SLIDER", // setting type
    "AISpawn - Máximo de unidades", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Falcon",
    [0, 300, 100, 0], // data for this setting: [min, max, default, number of shown trailing decimals]
    nil, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {  
        params ["_value"];
    } // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;


[
    "Falcon_debug_enabled", 
    "CHECKBOX", // setting type
    "Mensajes de Debug", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Falcon",
    false,
    true, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {  
        params ["_value"];
    } // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

[
    "Falcon_god_mode", 
    "CHECKBOX", // setting type
    ["Modo Dios", "Habilita modo dios para todos los jugadores"], 
    "Falcon",
    false,
    true, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {  
        params ["_value"];
        player allowDamage (!_value);
    } // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;

[
    "Falcon_always_teleport", 
    "CHECKBOX", // setting type
    "Teleport siempre disponible", // Pretty name shown inside the ingame settings menu. Can be stringtable entry.
    "Falcon",
    false,
    true, // "_isGlobal" flag. Set this to true to always have this setting synchronized between all clients in multiplayer
    {  
        params ["_value"];
        // show jip 
        if (_value) then {
            [player, true] call falcon_fnc_toggleTeleportAction;
        };
    } // function that will be executed once on mission start and every time the setting is changed.
] call CBA_fnc_addSetting;