#define DEBUG_MODULE "hostageRelease"
#include "..\..\base.hpp"


#define ATTACH_VECTOR [-.15, .75, 0]

if (!isServer) exitWith {
	DEBUG_LOG(DEBUG_MODULES, "ERROR: this function should only be called on the server!")
};

params ["_hostage"];


if (! (local _hostage) ) then {
	DEBUG_LOG(DEBUG_MODULE, "Server taking hostage ownership.");
	(group _hostage) setGroupOwner (clientOwner);
};

private _player = attachedTo _hostage;

if (!isNull _player) then {
	
	// Get 2d Dir normalized
	private _playerDir = (vectorDir _player);
	_playerDir set [2, 0];
	_playerDir = vectorNormalized _playerDir;


	// Get position 
	private _currentPosition = (getPos _player) vectorAdd [_playerDir#0 * ATTACH_VECTOR#0, _playerDir#1 * ATTACH_VECTOR#1, 0];

	// detach 
	detach _hostage;

	// Adjust position if not in vehicle
	if (vehicle _hostage == _hostage) then {
		_hostage setPos _currentPosition;
	};
};

_hostage setVariable ['falcon_is_captured', false, true];

_hostage allowDamage false;
_hostage setCaptive true;  
_hostage disableAI "ALL";  
_hostage enableAI "ANIM"; 
_hostage setCombatMode "BLUE";

// If the hostage was released inside a vehicle 
if ((vehicle _hostage) != _hostage) then {
	DEBUG_LOG(DEBUG_MODULE, "Hostage is in vehicle.");
	private _vehicle = vehicle _hostage;

	// update hostages in cargo 
	private _hostages = _vehicle getVariable ["falcon_hostage_cargo", []];
	_hostages pushBack _hostage;
	_vehicle setVariable ["falcon_hostage_cargo", _hostages, true];

	// Magic to prevent hostage from disembarking for no reason
	_hostage assignAsCargo _vehicle;
	_hostage moveInCargo _vehicle;
	_hostage setCombatMode "BLUE";
	_vehicle setUnloadInCombat [false, false];

	// Add disembark action to the vehicle only once
	// The action will hide/show when it has hostages inside 
	if (! (_vehicle getVariable ["falcon_hostage_disembark_action", false])) then {
		
		DEBUG_LOG(DEBUG_MODULE, "Adding vehicle disembark action.");

		_vehicle setVariable ["falcon_hostage_disembark_action", true, true];

		// Add action 
		[
			_vehicle,											// Object the action is attached to
			"Desembarcar Rehenes",											// Title of the action
			"\a3\data_f_destroyer\data\UI\IGUI\Cfg\holdactions\holdAction_unloadVehicle_ca.paa",	// Idle icon shown on screen
			"\a3\data_f_destroyer\data\UI\IGUI\Cfg\holdactions\holdAction_unloadVehicle_ca.paa",	// Progress icon shown on screen
			'((_this distance _target) < (sizeOf typeOf _target)/2) && (count (_target getVariable "falcon_hostage_cargo")) > 0',						// Condition for the action to be shown
			"((_caller distance _target) < (sizeOf typeOf _target)/2)",						// Condition for the action to progress
			{},													// Code executed when action starts
			{},													// Code executed on every progress tick
			{ 
				params ["_target", "_caller", "_actionId"];

				private _hostages = _target getVariable ["falcon_hostage_cargo", []];
				
				{
					unassignVehicle _x;
					moveOut _x;
				} forEach _hostages;
				

				_target setVariable ["falcon_hostage_cargo", [], true];

			}, // Code executed on completion
			{},													// Code executed on interrupted
			[],											// Arguments passed to the scripts as _this select 3
			3,													// Action duration in seconds
			0,													// Priority
			false,												// Remove on completion
			false,												// Show in unconscious state
			false												// show in window
		] remoteExec ["BIS_fnc_holdActionAdd", 0, _vehicle];
	};

};


DEBUG_LOG(DEBUG_MODULE, "Hostage released.");