#define DEBUG_MODULE "hostageReset"
#include "..\..\base.hpp"

#define IS_RESETTING "falcon_var_hostage_is_resetting"

params ["_hostage"];

if (!isServer) exitWith { true };

if (missionNamespace getVariable [IS_RESETTING, false]) exitWith { false };

missionNamespace setVariable [IS_RESETTING, true];

DEBUG_LOG(DEBUG_MODULE, "Started.");

{
	// client reset (exit hostage loop)
	_x setVariable ["falcon_has_hostage", false, owner _x];
	
} forEach allPlayers;


[_hostage] spawn {
	params ["_hostage"];

	sleep 3;


	unassignVehicle _hostage;

	if (vehicle _hostage != _hostage) then {
		moveOut _hostage;
	};

	sleep .5;

	// Teleport to admin position 
	private _adminPlayer = call falcon_fnc_getAdmin;
	if (! isNull _adminPlayer) then {
		_hostage setPos (getPos _adminPlayer);
	};


	missionNamespace setVariable [IS_RESETTING, false];
	DEBUG_LOG(DEBUG_MODULE, "Hostage reset.");
};

true