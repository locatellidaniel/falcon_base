#define DEBUG_MODULE "hostage"
#include "..\..\base.hpp"

if (!isServer) exitWith { true };

DEBUG_LOG(DEBUG_MODULE, "started.");

params ["_hostage"];

// initialize global module functions only once
if (isNil "falcon_fnc_hostage_capture") then {
    ["falconlib\scr\hostage\", "falcon_fnc_hostage_", [
        "captureClient",
		"embarkClient",
        "captureServer",
        "release",
        "reset"
    ]] call BIS_fnc_loadFunctions;
};

// init hostage

_hostage allowDamage false;
_hostage setCaptive true;  
_hostage disableAI "ALL";  
_hostage enableAI "ANIM"; 
_hostage setCombatMode "BLUE";

// Add action 
[
	_hostage,											// Object the action is attached to
	"Rescatar",											// Title of the action
	"\a3\missions_f_oldman\data\img\holdactions\holdAction_follow_start_ca.paa",	// Idle icon shown on screen
	"\a3\missions_f_oldman\data\img\holdactions\holdAction_follow_start_ca.paa",	// Progress icon shown on screen
	"(_this distance _target < 2) && !(_target getVariable ['falcon_is_captured', false])",						// Condition for the action to be shown
	"_caller distance _target < 2",						// Condition for the action to progress
	{},													// Code executed when action starts
	{},													// Code executed on every progress tick
	{ 
		params ["_target", "_caller"];
		[_target, _caller] remoteExec ["falcon_fnc_hostage_captureServer", 2];
	}, // Code executed on completion
	{},													// Code executed on interrupted
	[_hostage],											// Arguments passed to the scripts as _this select 3
	3,													// Action duration in seconds
	0,													// Priority
	false,												// Remove on completion
	false												// Show in unconscious state
] remoteExec ["BIS_fnc_holdActionAdd", 0, _hostage];	// MP compatible implementation

