if (!canSuspend) exitWith { _this spawn falcon_fnc_hostage_embarkClient };

params ["_hostage", "_vehicle"];


_hostage assignAsCargo _vehicle;
_hostage moveInCargo _vehicle;


sleep 1;	// we have to wait server sync to check again

// release hostage if successful
if (vehicle _hostage != _hostage) then {
	player setVariable ["falcon_has_hostage", false];
};