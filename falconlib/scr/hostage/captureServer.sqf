#define DEBUG_MODULE "hostageCaptureServer"
#include "..\..\base.hpp"


if (!isServer) exitWith {};
DEBUG_LOG(DEBUG_MODULE, "Started.");

params ["_hostage", "_player"];


(group _hostage) setGroupOwner (owner _player);

_hostage setVariable ['falcon_is_captured', true, true];

[_hostage] remoteExec ["falcon_fnc_hostage_captureClient", owner _player];