#define DEBUG_MODULE "hostageCaptureClient"
#include "..\..\base.hpp"

#define GET_SPEED falcon_fn_hostage_getPlayerSpeedMode
#define UPDATE_HOSTAGE_SPEED falcon_fn_hostage_updateSpeedMode
#define HOSTAGE_RELEASE falcon_fn_hostage_release
#define HAS_HOSTAGE falcon_has_hostage
#define CAN_EMBARK falcon_can_embark_hostage

#define ATTACH_VECTOR [-.15, .75, 0]

DEBUG_LOG(DEBUG_MODULE, "Started");

params ["_hostage"];

_hostage allowDamage false;
_hostage setCaptive true;  
_hostage disableAI "ALL";  
_hostage enableAI "ANIM"; 
_hostage setCombatMode "BLUE";
	
_stance = stance player; 
_pose = pose player;


GET_SPEED = {
	private _s = speed player;

	if (_s == 0) exitWith {0};
	if (_s < 7.0) exitWith {1};
	if (_s < 15.0) exitWith {2};

	3;
};

UPDATE_HOSTAGE_SPEED = {
	params ["_hostage", "_speed"];
	
	if (_speed == 0) then {
		_hostage playActionNow "Crouch";
	};
	if (_speed == 1) then {
		_hostage playActionNow "WalkF";
	};
	if (_speed == 2) then {
		_hostage playActionNow "SlowF";
	};
	if (_speed == 3) then {
		_hostage playActionNow "FastF";
	};
};

player setVariable ["falcon_has_hostage", true];
player setVariable ["falcon_hostage", _hostage];

private _speed = call GET_SPEED; 
private _pose = (pose player);

_hostage attachTo [player, ATTACH_VECTOR]; 
_hostage setUnitPos "MIDDLE";

// Update hostage animation eh
private _eh_hostage_animDone = _hostage addEventHandler ["AnimChanged", {
	params ["_unit", "_anim"];
	private _speed = call GET_SPEED;

	if (_speed > 0) then {
		[_unit, _speed] call UPDATE_HOSTAGE_SPEED;
	};

}];

private _eh_player_getin = player addEventHandler ["GetInMan", {
	params ["_unit", "_role", "_vehicle", "_turret"];

	// emabrk the hostage first
	[player getVariable "falcon_hostage", _vehicle] call falcon_fnc_hostage_embarkClient;
}];

// release hostage when player is killed
private _eh_player_killed = player addEventHandler ["Killed", {
	player setVariable ["falcon_has_hostage", false];
}];




// Release action
private _idActionFree = [
	player,												// Object the action is attached to
	"Liberar Rehén",									// Title of the action
	"\a3\missions_f_oldman\data\img\holdactions\holdAction_follow_stop_ca.paa",	// Idle icon shown on screen
	"\a3\missions_f_oldman\data\img\holdactions\holdAction_follow_stop_ca.paa",	// Progress icon shown on screen
	'true',						// Condition for the action to be shown
	"true",						// Condition for the action to progress
	{},													// Code executed when action starts
	{},													// Code executed on every progress tick
	{ 
		params ["_target", "_caller", "_actionId"];

		player setVariable ["falcon_has_hostage", false];

	}, // Code executed on completion
	{},													// Code executed on interrupted
	[],													// Arguments passed to the scripts as _this select 3
	3,													// Action duration in seconds
	6,													// Priority
	false,												// Remove on completion
	false,												// Show in unconscious state
	false												// show on the middle of the screen
] call BIS_fnc_holdActionAdd;



// Embark action
private _idActionEmbark = [
	player,												// Object the action is attached to
	"Embarcar Rehén",									// Title of the action
	"\a3\data_f_destroyer\data\UI\IGUI\Cfg\holdactions\holdAction_loadVehicle_ca.paa",	// Idle icon shown on screen
	"\a3\data_f_destroyer\data\UI\IGUI\Cfg\holdactions\holdAction_loadVehicle_ca.paa",	// Progress icon shown on screen
	'((player distance cursorTarget) < (sizeOf typeOf cursorTarget)/2) && (cursorTarget isKindOf "Car" || cursorTarget isKindOf "Plane" || cursorTarget isKindOf "Helicopter" || cursorTarget isKindOf "Ship")',						// Condition for the action to be shown
	"true",						// Condition for the action to progress
	{},													// Code executed when action starts
	{},													// Code executed on every progress tick
	{ 
		params ["_target", "_caller", "_actionId", "_arguments"];

		private _hostage = _arguments#0;
		private _vehicle = cursorTarget;
		
		[_hostage, _vehicle] call falcon_fnc_hostage_embarkClient;

	}, // Code executed on completion
	{},													// Code executed on interrupted
	[_hostage],											// Arguments passed to the scripts as _this select 3
	3,													// Action duration in seconds
	6,													// Priority
	false,												// Remove on completion
	false,												// Show in unconscious state
	true												
] call BIS_fnc_holdActionAdd;



while { player getVariable ["falcon_has_hostage", false] } do {  
	if (stance player != _stance) then { 
		_stance = stance player; 
		
		if (_stance == "STAND") then {  
			_hostage setUnitPos "MIDDLE";          
		};  
		if (_stance == "CROUCH") then {  
			_hostage setUnitPos "DOWN";          
		};  
		
	}; 

	private _currentSpeedMode = call GET_SPEED;

	if (_speed != _currentSpeedMode) then {
		_speed = _currentSpeedMode;
		
		[_hostage, _speed] call UPDATE_HOSTAGE_SPEED;
	};
		
	// release hostage when player is injured
	if ((lifeState player) == "INCAPACITATED") then {
		player setVariable ["falcon_has_hostage", false];
	};

	sleep .25;  
};  


DEBUG_LOG(DEBUG_MODULE, "Removing actions...");

[player, _idActionFree ] call BIS_fnc_holdActionRemove;
[player, _idActionEmbark ] call BIS_fnc_holdActionRemove;


_hostage removeEventHandler ["AnimChanged", _eh_hostage_animDone];
player removeEventHandler ["GetInMan", _eh_player_getin];
player removeEventHandler ["Killed", _eh_player_killed];


player setVariable ["falcon_hostage", nil];

// tell the server to release the hostage
_hostage remoteExec ["falcon_fnc_hostage_release", 2];