/*
	fn_fireArtillery

	Orders an artillery group to fire to a position. Selects apropiate ammunition.

	Usage:

	_trigger		trigger
	_units			artillery units.
	_rounds			# of rounds. 0 means fire until trigger is deactivated.
	_wait			seconds between rounds
	_ammo			[optional] Ammo magazine to use

	Example:

	[thisTrigger, unitEnemyArtillery_1, 1, 20] execVM "falconlib\src\ambientArtillery.sqf";

	Notes: execute where artillery is local

	Author: JohnMisil

 */

#define DEBUG_MODULE "ambientArtillery"
#include "..\base.hpp"

#define RANDOM_WAIT 10

DEBUG_LOG(DEBUG_MODULE, "started.");
params ["_trigger", "_units", "_rounds", "_wait", "_ammo"];

while {triggerActivated _trigger} do {
	scopeName "whileActive";
	private ["_position", "_aliveCount"];
	_position = [_trigger] call falcon_fnc_getRandomCoordinates;

	// count for alive units
	_aliveCount = {alive _x} count _units;
	if (_aliveCount == 0) then { breakOut "whileActive"; };

	{
		private ["_unit", "_mag"];
		_unit = _x;

		_mag = if (!isNil "_ammo") then {_ammo};

		// find the first available ammo. Careful it can be smoke, flares or mines.

		if (isNil "_mag") then {
			_mag = {
				if (_position inRangeOfArtillery [[_unit], _x]) exitWith { _x };
			} forEach getArtilleryAmmo [_unit];
		};

		if (isNil "_mag" || !alive _unit) then {
			DEBUG_LOG(DEBUG_MODULE, "No fire. Check range, mag or alive.");
		} else {
			if (_mag == "") then { DEBUG_LOG(DEBUG_MODULE, "wtf"); };
			_unit doArtilleryFire [_position, _mag, _rounds];
			DEBUG_LOG(DEBUG_MODULE, "Fired! " + _mag);
			sleep 5;
			// reload
			_unit addMagazineTurret [_mag, [0]];
		};

	} forEach _units;
	DEBUG_LOG(DEBUG_MODULE, "waiting");
	sleep _wait + (random RANDOM_WAIT) - RANDOM_WAIT/2;
};

DEBUG_LOG(DEBUG_MODULE, "Finished firing");
