// Directives for description.ext
//
//

#include "base.hpp"

enableDebugConsole = 1; // only admins

// Dialogs
#include "rsc\bisBaseControls.cpp"

#include "rsc\dialogJIP.cpp"
#include "rsc\dialogCajaRadios.cpp"
#include "rsc\dialogCajaUniformes.cpp"

class CfgFunctions
{
	#include "rsc\libFunctions.hpp"
    #if __has_include("..\local\CfgFunctions.hpp")
    #include "..\local\CfgFunctions.hpp"
    #endif
};

class CfgNotifications
{
	#include "rsc\notifications.hpp"
    #if __has_include("..\local\CfgNotifications.hpp")
    #include "..\local\CfgNotifications.hpp"
    #endif
};

class CfgSounds
{
	sounds[] = {};
	#include "rsc\sounds.hpp"
    #if __has_include("..\local\CfgSounds.hpp")
    #include "..\local\CfgSounds.hpp"
    #endif
};

// Params
class Params
{
    #include "rsc\falconBaseParams.hpp"
    #include "\a3\Functions_F\Params\paramRevive.hpp"
    #if __has_include("..\local\Params.hpp")
    #include "..\local\Params.hpp"
    #endif
};

class Extended_PreInit_EventHandlers {
    class falcon_pre_init_event {
        init = "call compile preprocessFileLineNumbers 'falconlib\scr\XEH_preInit.sqf'";
    };
};


// Titles

class RscTitles {
    titles[] = {};
    
    #include "rsc\titleFalconIntroPlain.cpp"
};
